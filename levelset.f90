! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module levelset
  use communication
  use polynomial
  use variables
  use velocity
  use basis
  use grid
  implicit none

  real(WP) :: Gconverge

end module levelset

!===================================!
!          Initialization           !
!===================================!
subroutine levelset_init
  use levelset
  use math
  use io
  implicit none

  integer :: i

  !============= Allocate level set arrays ============!
  ! Level Set
  allocate(G      (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gold   (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gupx   (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gupy   (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(G_now  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(rhsG   (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gsave  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gprob  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gsum   (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  ! Derivatives and interpolation arrays
  allocate(Gxi  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gyi  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gco  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gdx  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gdy  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gdxi (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gdyi (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  ! Compression
  allocate(comp  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(comppi(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(compmi(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(comppj(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(compmj(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  ! Diffusion
  allocate(diff  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(diff2 (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(diffpi(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(diffmi(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(diffpj(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(diffmj(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  ! Normals
  allocate(Nox (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Noy (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Nxi (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Nyi (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Nxv (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Nyh (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  ! Kinetic Energy
  allocate(KE (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(gKE(1:niter,1:ndof+1))
  ! Interfacial Area
  allocate(iVol(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(giVol(0:niter,2))
  ! Romberg Integration Array
  allocate(ri(500,500))
  allocate(riV(imino_:imaxo_,jmino_:jmaxo_,500,500))

  ! Initialize arrays
  G = 0.0_WP; G_now = 0.0_WP; rhsG = 0.0_WP

  ! Set epsilon values for reinitialization (0.85 & 0.15) (1.125 & 0.125) 
  epsG  = 0.875_WP*max(dx,dy)
  epsG2 = 0.125_WP*max(dx,dy)

  ! Calculate normal scalar and initial values
  norm = 1.0_WP/(4.0_WP*(epsG+epsG2))

  ! Initialize values for normal calculations
  delt = floor(real(gridbase,WP)/2.0_WP)
  ! Central point xm(i) and ym(j) are average (0)
  botxi = 1.0_WP/(real(gridbase,WP)*sum((xm(imin_-delt:imin_+delt)-xm(imin_))**2)*norm)
  botyi = 1.0_WP/(real(gridbase,WP)*sum((ym(jmin_-delt:jmin_+delt)-ym(jmin_))**2)*norm)

  ! Import Geometry
  call geometry_init
  call comm_borders(G)
  Gsave = G

  ! Set Convergence
  Gconverge = 0.00000000000001_WP

  ! Initial level set convergence
  dtre = reCFL*min(min(dx,dy),min(dx**2,dy**2)/(4.0_WP*(epsG+epsG2)))
  do i=1,ilsc
     call levelset_reinit
  end do

  ! Calculate initial surface tension
  call levelset_curvature

  ! Find initial probabilities
  call levelset_probability

  ! Save interfacial fluid mass (for conservation tracking)
  AvgIntDiff = 0.0_WP
  call data_volume

  return
end subroutine levelset_init

!=============================================!
!          Level Set RHS - FLUX-HOUC          !
!=============================================!
subroutine levelset_rhs
  use levelset
  implicit none

  integer :: i,j,tt,k,l

  ! Zero arrays
  rhsG = 0.0_WP; Gupx = 0.0_WP; Gupy = 0.0_WP

  ! Calculate Upwinded Level Set
  ! Interpolates level set values to left and bottom cell edges
  do j=jmin_,jmax_+1
     do i=imin_,imax_+1
        do k=1,ndof
           ! G at left cell face
           if (u(i,j,1).ge.0.0_WP) then
              Gupx(i,j,k)=sum(houc_xp*G(i-ceiling(real(nhouc,WP)/2.0_WP):i+(floor(real(nhouc,WP)/2.0_WP)-1),j,k))
           else
              Gupx(i,j,k)=sum(houc_xm*G(i-floor(real(nhouc,WP)/2.0_WP):i+floor(real(nhouc,WP)/2.0_WP),j,k))
           end if
           ! G at bottom cell face
           if (v(i,j,1).ge.0.0_WP) then
              Gupy(i,j,k)=sum(houc_yp*G(i,j-ceiling(real(nhouc,WP)/2.0_WP):j+(floor(real(nhouc,WP)/2.0_WP)-1),k))
           else
              Gupy(i,j,k)=sum(houc_ym*G(i,j-floor(real(nhouc,WP)/2.0_WP):j+floor(real(nhouc,WP)/2.0_WP),k))
           end if
        end do
     end do
  end do

  ! Communication
  call comm_borders(Gupx)
  call comm_borders(Gupy)

  ! Calculate needed information
  Gdx = 0.0_WP; Gdy = 0.0_WP
  Gdx(imin_:imax_,:,:) = (Gupx(imin_+1:imax_+1,:,:)-Gupx(imin_:imax_,:,:))*dxi
  Gdy(:,jmin_:jmax_,:) = (Gupy(:,jmin_+1:jmax_+1,:)-Gupy(:,jmin_:jmax_,:))*dyi
  call comm_borders(Gdx); call comm_borders(Gdy)
  Uint = 0.0_WP; Vint = 0.0_WP
  Uint(imin_:imax_,:,:) = 0.5_WP*(u(imin_:imax_,:,:) + u(imin_+1:imax_+1,:,:))
  Vint(:,jmin_:jmax_,:) = 0.5_WP*(v(:,jmin_:jmax_,:) + v(:,jmin_+1:jmax_+1,:))
  call comm_borders(Uint); call comm_borders(Vint)

  ! Compute RHS by looping over Mklt
  ! Relocates level set values to cell center
  do i = 1,Mn
     k = Mlsave(i,1); l = Mlsave(i,2); tt = Mlsave(i,3)
     ! Add terms to RHS
     rhsG(:,:,tt) = rhsG(:,:,tt) - (Uint(:,:,k)*Gdx(:,:,l)+Vint(:,:,k)*Gdy(:,:,l))*M(k,l,tt)
  end do

  ! Communication
  call comm_borders(rhsG)

  return
end subroutine levelset_rhs

!==============================================!
!          Level Set Reinitialization          !
!==============================================!
subroutine levelset_reinit
  use levelset
  use grid
  implicit none
  real(WP) :: myRes,Res

  ! Reinitialize with a Crank-Nicolson
  G_now = G; Res = 1.0_WP; Gold = 0.0_WP; ctr2 = 0
  do while ((Res.gt.Gconverge).and.(ctr2.lt.cni))
     ctr2 = ctr2 + 1

     ! Calculate half-step
     G = 0.5_WP*(G_now + G)
     call comm_borders(G)

     ! Calculate information
     call levelset_normal
     call levelset_diffcomp

     ! Update level set
     G = G_now + dtre*(comp+diff)

     ! Communicate
     call comm_borders(G)

     ! Enforce boundary conditions
     call geometry_bc

     ! End cycle of reinitialization
     myRes=maxval(abs(G-Gold))
     call max_real(myRes,Res)
     Gold = G
  end do

  return
end subroutine levelset_reinit

!==============================================!
!           Level Set Normal Vector            !
!==============================================!
subroutine levelset_normal
  use levelset
  use math
  implicit none
  integer :: i,j,k
  ! real(WP) :: Cnorm_local

  ! Zero out arrays
  Nox = 0.0_WP; Noy = 0.0_WP

  ! Calculate Normals with a 2-D Least Squares Approach
  ! Calculated at the cell center (where G is located)
  ! Only works with a structured, non-variable grid
  do i = imin_-1,imax_+1
     do j = jmin_-1,jmax_+1
        do k = 1,ndof
           ! Calculate top of fraction with average values
           ! dG/dx
           topx = sum(matmul((xm(i-delt:i+delt)-xm(i)),G(i-delt:i+delt,j-delt:j+delt,k)))
           ! dG/dy
           topy = sum(matmul((ym(j-delt:j+delt)-ym(j)),transpose(G(i-delt:i+delt,j-delt:j+delt,k))))
           ! Calculate Normals
           Nox(i,j,k) = -topx*botxi
           Noy(i,j,k) = -topy*botyi
        end do
     end do
  end do

  ! Communication
  call comm_borders(Nox); call comm_borders(Noy)

  ! Calculates left cell face at Nox(i), Noy(i)
  Nxi = 0.0_WP; Nyh = 0.0_WP
  Nxi(imin_:imax_+1,:,:) = 0.5_WP*(Nox(imin_:imax_+1,:,:)+Nox(imin_-1:imax_,:,:))
  Nyh(imin_:imax_+1,:,:) = 0.5_WP*(Noy(imin_:imax_+1,:,:)+Noy(imin_-1:imax_,:,:))
  call comm_borders(Nxi); call comm_borders(Nyh)

  ! Calculates bottom cell face at Noy(j), Nox(j)
  Nyi = 0.0_WP; Nxv = 0.0_WP
  Nyi(:,jmin_:jmax_+1,:) = 0.5_WP*(Noy(:,jmin_:jmax_+1,:)+Noy(:,jmin_-1:jmax_,:))
  Nxv(:,jmin_:jmax_+1,:) = 0.5_WP*(Nox(:,jmin_:jmax_+1,:)+Nox(:,jmin_-1:jmax_,:))
  call comm_borders(Nyi); call comm_borders(Nxv)

  ! ! Calculate normal correction
  ! Cnorm_local = maxval(sqrt((Nox(:,:,1)*norm)**2 + (Noy(:,:,1)*norm)**2))
  ! call max_real(Cnorm_local, C_norm)
  ! C_norm = 2.0_WP*C_norm*(epsG + epsG2) - 0.5_WP
  C_norm = 0.0_WP

  return
end subroutine levelset_normal

!====================================================!
!     Reinitialization Diffusion and Compression     !
!====================================================!
subroutine levelset_diffcomp
  use levelset
  use io
  implicit none

  integer :: tt,k,l,mm,i

  ! Zero out the terms
  ! Diffusion
  diffpi = 0.0_WP; diffmi = 0.0_WP; diff2 = 0.0_WP
  diffpj = 0.0_WP; diffmj = 0.0_WP; diff  = 0.0_WP
  ! Compression
  comppi = 0.0_WP; compmi = 0.0_WP; comp  = 0.0_WP
  comppj = 0.0_WP; compmj = 0.0_WP

  ! Calculates left and bottom cell face at G(i), G(j)
  Gxi = 0.0_WP; Gyi = 0.0_WP
  Gxi(imin_:imax_+1,:,:) = 0.5_WP*(G(imin_:imax_+1,:,:)+G(imin_-1:imax_,:,:))
  Gyi(:,jmin_:jmax_+1,:) = 0.5_WP*(G(:,jmin_:jmax_+1,:)+G(:,jmin_-1:jmax_,:))
  call comm_borders(Gxi); call comm_borders(Gyi)

  ! Calculate partial derivatives (at cell walls) !!!!!
  ! Calculates left cell face at dGdx(i), dGdy(i)
  Gdx = 0.0_WP; Gdyi = 0.0_WP
  Gdx(imin_:imax_+1,:,:)  = (G(imin_:imax_+1,:,:)-G(imin_-1:imax_,:,:))*dxi
  Gdyi(:,jmin_:jmax_,:) = 0.5_WP*(Gxi(:,jmin_+1:jmax_+1,:)-Gxi(:,jmin_-1:jmax_-1,:))*dyi
  call comm_borders(Gdx); call comm_borders(Gdyi)

  ! Calculates bottom cell face at dGdy(j), dGdx(j)
  Gdy = 0.0_WP; Gdxi = 0.0_WP
  Gdy(:,jmin_:jmax_+1,:)  = (G(:,jmin_:jmax_+1,:)-G(:,jmin_-1:jmax_,:))*dyi
  Gdxi(imin_:imax_,:,:) = 0.5_WP*(Gyi(imin_+1:imax_+1,:,:)-Gyi(imin_-1:imax_-1,:,:))*dxi
  call comm_borders(Gdy); call comm_borders(Gdxi)

  ! Loop over basis functions to calculate each term
  ! Final derivative occurs over a single cell
  do i = 1,Mn
     k = Mlsave(i,1); l = Mlsave(i,2); tt = Mlsave(i,3)
     ! Compression calculation
     ! Calculate G*1*Nx
     ! Calculate d/dx
     ! Calculates the i+1 (right) side of the cell
     comppi(imin_:imax_,:,tt) = comppi(imin_:imax_,:,tt) + (1.0_WP + C_norm)* &
          Gxi(imin_+1:imax_+1,:,k)*Nxi(imin_+1:imax_+1,:,l)*M(k,l,tt)
     ! Calculates the i (left) side of the cell
     compmi(imin_:imax_,:,tt) = compmi(imin_:imax_,:,tt) + (1.0_WP + C_norm)* &
          Gxi(imin_:imax_,:,k)*Nxi(imin_:imax_,:,l)*M(k,l,tt)
     ! Calculate d/dy
     ! Calculates the j+1 (top) side of the cell
     comppj(:,jmin_:jmax_,tt) = comppj(:,jmin_:jmax_,tt) + (1.0_WP + C_norm)* &
          Gyi(:,jmin_+1:jmax_+1,k)*Nyi(:,jmin_+1:jmax_+1,l)*M(k,l,tt)
     ! Calculates the j (bottom) side of the cell
     compmj(:,jmin_:jmax_,tt) = compmj(:,jmin_:jmax_,tt) + (1.0_WP + C_norm)* &
          Gyi(:,jmin_:jmax_,k)*Nyi(:,jmin_:jmax_,l)*M(k,l,tt)
  end do
  do i = 1,Dn
     k = Dlsave(i,1); l = Dlsave(i,2); mm = Dlsave(i,3); tt = Dlsave(i,4)
     ! Compression calculation
     ! Calculate G*-G*Nx
     ! Calculate d/dx
     ! Calculates the i+1 (right) side of the cell
     comppi(imin_:imax_,:,tt) = comppi(imin_:imax_,:,tt) + &
          Gxi(imin_+1:imax_+1,:,k)*(-Gxi(imin_+1:imax_+1,:,mm))*Nxi(imin_+1:imax_+1,:,l)*Dklmt(k,l,mm,tt)
     ! Calculates the i (left) side of the cell
     compmi(imin_:imax_,:,tt) = compmi(imin_:imax_,:,tt) + &
          Gxi(imin_:imax_,:,k)*(-Gxi(imin_:imax_,:,mm))*Nxi(imin_:imax_,:,l)*Dklmt(k,l,mm,tt)
     ! Calculate d/dy
     ! Calculates the j+1 (top) side of the cell
     comppj(:,jmin_:jmax_,tt) = comppj(:,jmin_:jmax_,tt) + &
          Gyi(:,jmin_+1:jmax_+1,k)*(-Gyi(:,jmin_+1:jmax_+1,mm))*Nyi(:,jmin_+1:jmax_+1,l)*Dklmt(k,l,mm,tt)
     ! Calculates the j (bottom) side of the cell
     compmj(:,jmin_:jmax_,tt) = compmj(:,jmin_:jmax_,tt) + &
          Gyi(:,jmin_:jmax_,k)*(-Gyi(:,jmin_:jmax_,mm))*Nyi(:,jmin_:jmax_,l)*Dklmt(k,l,mm,tt)
     ! Diffusion Calculation
     ! Calculate the d/dx term
     ! Calculate the i+1 (right) side of the cell
     diffpi(imin_:imax_,:,tt) = diffpi(imin_:imax_,:,tt) + &
          (Gdx(imin_+1:imax_+1,:,k)*Nxi(imin_+1:imax_+1,:,l)*Nxi(imin_+1:imax_+1,:,mm) + &
          Gdyi(imin_+1:imax_+1,:,k)*Nyh(imin_+1:imax_+1,:,l)*Nxi(imin_+1:imax_+1,:,mm))*Dklmt(k,l,mm,tt)
     ! Calculate the i (left) side of the cell
     diffmi(imin_:imax_,:,tt) = diffmi(imin_:imax_,:,tt) + &
          (Gdx(imin_:imax_,:,k)*Nxi(imin_:imax_,:,l)*Nxi(imin_:imax_,:,mm) + &
          Gdyi(imin_:imax_,:,k)*Nyh(imin_:imax_,:,l)*Nxi(imin_:imax_,:,mm))*Dklmt(k,l,mm,tt)
     ! Calculate the d/dy term
     ! Calculate the j+1 (top) side of the cell
     diffpj(:,jmin_:jmax_,tt) = diffpj(:,jmin_:jmax_,tt) + &
          (Gdxi(:,jmin_+1:jmax_+1,k)*Nxv(:,jmin_+1:jmax_+1,l)*Nyi(:,jmin_+1:jmax_+1,mm) + &
          Gdy(:,jmin_+1:jmax_+1,k)*Nyi(:,jmin_+1:jmax_+1,l)*Nyi(:,jmin_+1:jmax_+1,mm))*Dklmt(k,l,mm,tt)
     ! Calculate the j (bottom) side of the cell
     diffmj(:,jmin_:jmax_,tt) = diffmj(:,jmin_:jmax_,tt) + &
          (Gdxi(:,jmin_:jmax_,k)*Nxv(:,jmin_:jmax_,l)*Nyi(:,jmin_:jmax_,mm) + &
          Gdy(:,jmin_:jmax_,k)*Nyi(:,jmin_:jmax_,l)*Nyi(:,jmin_:jmax_,mm))*Dklmt(k,l,mm,tt)
  end do

  ! Calculate a general diffusion term
  diff2(imin_:imax_,jmin_:jmax_,:) = &
       ((Gdx(imin_+1:imax_+1,jmin_:jmax_,:)-Gdx(imin_:imax_,jmin_:jmax_,:))*dxi + &
       (Gdy(imin_:imax_,jmin_+1:jmax_+1,:)-Gdy(imin_:imax_,jmin_:jmax_,:))*dyi)

  ! Calculate the entire diffusion term
  diff = epsG*(diffpi-diffmi)*dxi + epsG*(diffpj-diffmj)*dyi + epsG2*diff2
  ! epsG2*diff2*(1.0_WP-Nox*Nox-Noy*Noy) ! Potential to diffuse away from interface

  ! Calculate entire compression term
  comp = (comppi-compmi)*dxi + (comppj-compmj)*dyi

  ! Communicate
  call comm_borders(diff)
  call comm_borders(comp)

  return
end subroutine levelset_diffcomp

!=========================================!
!          Curvature Calculation          !
!=========================================!
subroutine levelset_curvature
  use levelset
  use math
  use io
  implicit none

  integer :: k,l,mm,tt,i
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,1:ndof) :: Ksx_buf, Ksy_buf
  ! real(WP) :: surfT

  ! Check if using multiphase
  if (.not.use_multiphase) return

  ! Calculate density, viscosity
  call velocity_rhonu

  ! Calculate gradient, unit normals
  call levelset_gradient

  ! Interpolate curvature to cell walls
  Ksx_buf = 0.0_WP; Ksy_buf = 0.0_WP
  Ksx_buf(imin_:imax_+1,:,:) = 0.5_WP*(Ks(imin_-1:imax_,:,:) + Ks(imin_:imax_+1,:,:))
  Ksy_buf(:,jmin_:jmax_+1,:) = 0.5_WP*(Ks(:,jmin_-1:jmax_,:) + Ks(:,jmin_:jmax_+1,:))
  call comm_borders(Ksx_buf); call comm_borders(Ksy_buf)

  ! Find surface tension (at cell walls)
  Kx = 0.0_WP; Ky = 0.0_WP
  do i = 1,Dn
     k = Dlsave(i,1); l = Dlsave(i,2); mm = Dlsave(i,3); tt = Dlsave(i,4)
     Kx(:,:,tt) = Kx(:,:,tt) + Ksx_buf(:,:,k)*Kxx(:,:,l)*gammak(mm)*Dklmt(k,l,mm,tt)
     Ky(:,:,tt) = Ky(:,:,tt) + Ksy_buf(:,:,k)*Kyy(:,:,l)*gammak(mm)*Dklmt(k,l,mm,tt)
  end do
  call comm_borders(Kx); call comm_borders(Ky)

  ! ! Try a curvature scheme (subtracts directional derivative)
  ! Gsave = 0.0_WP
  ! Gsave(imin_:imax_,jmin_:jmax_,:) = &
  !      0.5_WP*(Nox(imin_+1:imax_+1,jmin_:jmax_,:)-Nox(imin_-1:imax_-1,jmin_:jmax_,:))*dxi + &
  !      0.5_WP*(Noy(imin_:imax_,jmin_+1:jmax_+1,:)-Noy(imin_:imax_,jmin_-1:jmax_-1,:))*dyi
  ! Gsave = Gsave - Nox*Gdx - Noy*Gdy
  ! call comm_borders(Gsave)

  ! ! Check surface tension
  ! surfT = sum(Kx(nx/2:imax_,ny/2,:)*dx) + sum(Ky(nx/2:imax_,ny/2,:)*dx)
  ! print*,'Surface Tension = ',real(surfT,SP)

  return
end subroutine levelset_curvature

!========================================!
!          Gradient Calculation          !
!========================================!
subroutine levelset_gradient
  use levelset
  use math
  use io
  implicit none

  integer :: i,k
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: NxQ, NyQ

  ! Calculate 1st derivative at cell center
  Gdx = 0.0_WP; Gdy = 0.0_WP
  Gdx(imin_-2:imax_+2,:,:) = 0.5_WP*(G(imin_-1:imax_+3,:,:)-G(imin_-3:imax_+1,:,:))*dxi
  Gdy(:,jmin_-2:jmax_+2,:) = 0.5_WP*(G(:,jmin_-1:jmax_+3,:)-G(:,jmin_-3:jmax_+1,:))*dyi
  call comm_borders(Gdx); call comm_borders(Gdy)

  ! Calculate curvature with quadrature
  Ks = 0.0_WP; NxQ = 0.0_WP; NyQ = 0.0_WP
  do k = 1,ndof
     do i = 1,quadpts
        NxQ = poly_eval3(Gdx,i)/sqrt(poly_eval3(Gdx,i)**2 + poly_eval3(Gdy,i)**2)
        NyQ = poly_eval3(Gdy,i)/sqrt(poly_eval3(Gdx,i)**2 + poly_eval3(Gdy,i)**2)
        where (abs((0.5_WP*(NxQ(imin_:imax_+2,jmin_-1:jmax_+1)-NxQ(imin_-2:imax_,jmin_-1:jmax_+1))*dxi + &
             0.5_WP*(NyQ(imin_-1:imax_+1,jmin_:jmax_+2)-NyQ(imin_-1:imax_+1,jmin_-2:jmax_))*dyi)) &
             .lt.(1.0_WP/min(dx,dy)))
           Ks(imin_-1:imax_+1,jmin_-1:jmax_+1,k) = Ks(imin_-1:imax_+1,jmin_-1:jmax_+1,k) + Wgauss(i)*Quad_bas(k,i)* &
                (0.5_WP*(NxQ(imin_:imax_+2,jmin_-1:jmax_+1)-NxQ(imin_-2:imax_,jmin_-1:jmax_+1))*dxi + &
                0.5_WP*(NyQ(imin_-1:imax_+1,jmin_:jmax_+2)-NyQ(imin_-1:imax_+1,jmin_-2:jmax_))*dyi)
        end where
     end do
     Ks(:,:,k) = -Ks(:,:,k)/Var(k)
  end do
  call comm_borders(Ks)

  ! Calculate Gradient at cell walls with level set
  Kxx = 0.0_WP; Kyy = 0.0_WP
  Kxx(imin_:imax_+1,:,:) = (G(imin_:imax_+1,:,:)-G(imin_-1:imax_,:,:))*dxi
  Kyy(:,jmin_:jmax_+1,:) = (G(:,jmin_:jmax_+1,:)-G(:,jmin_-1:jmax_,:))*dyi
  call comm_borders(Kxx); call comm_borders(Kyy)

  return
end subroutine levelset_gradient

!==============================!
!        Kinetic Energy        !
!==============================!
subroutine KineticEnergy
  use levelset
  use math
  implicit none

  real(WP) :: gKE_local
  integer :: kk,ll,mm,tt,i

  ! Interpolate velocity to cell center
  Uint = 0.0_WP; Vint = 0.0_WP
  Uint(imin_:imax_,:,:) = 0.5_WP*(u(imin_+1:imax_+1,:,:)+u(imin_:imax_,:,:))
  Vint(:,jmin_:jmax_,:) = 0.5_WP*(v(:,jmin_+1:jmax_+1,:)+v(:,jmin_:jmax_,:))
  call comm_borders(Uint)
  call comm_borders(Vint)

  ! Calculate Kinetic Energy with a dot product u dot u = u^2
  KE = 0.0_WP
  do i = 1,Dn
     kk = Dlsave(i,1); ll = Dlsave(i,2); mm = Dlsave(i,3); tt = Dlsave(i,4)
     KE(:,:,tt) = KE(:,:,tt) + 0.5_WP*rhok(:,:,kk)*(Uint(:,:,ll)*Uint(:,:,mm) + &
          Vint(:,:,ll)*Vint(:,:,mm))*Dklmt(kk,ll,mm,tt)
  end do
  call comm_borders(KE)

  ! Calculate Global Kinetic Energy
  gKE(iter,1) = time
  do tt = 1,ndof
     gKE_local = sum(KE(imin_:imax_,jmin_:jmax_,tt)*dx*dy)
     call sum_real(gKE_local,gKE(iter,tt+1))
  end do

  return
end subroutine KineticEnergy

!====================================!
!      Probability Calculation       !
!====================================!
subroutine levelset_probability
  use levelset
  use math
  use io
  implicit none
  integer :: l
  real(WP) :: zstep

  ! Calculate zeta step size
  zstep = (zetamax-zetamin)/100.0_WP

  ! Calculates the probability of being liquid (G > 0.5)
  ! Uses a systematic sampling scheme over zeta (100 sample points)
  Gprob = 0.0_WP
  do l = 0,100
     where ((poly_eval2(G,(zetamin+zstep*real(l,WP)))).gt.0.5_WP) 
        Gprob(:,:,1) = Gprob(:,:,1) + 1.0_WP
     end where
  end do
  Gprob = Gprob/101.0_WP

  ! Communicate
  call comm_borders(Gprob)

  return
end subroutine levelset_probability








