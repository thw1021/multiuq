! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module geometry
  use communication
  use polynomial
  use variables
  use levelset
  use basis
  use math
  use grid
  use io
  implicit none

  ! Used to initialize level set profiles

end module geometry

!==================================!
!     Geometry Initialization      !
!==================================!
subroutine geometry_init
  use geometry
  implicit none

  integer :: i,j
  integer :: verr
  real(WP) :: theta, xe

  ! Set Map to Zero
  G = 0.0_WP

  ! Create initial level set - signed distance function
  select case(trim(Gshape))

  case ('Circle','circle','CIRCLE')
     ! Create distance level set
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           G(i,j,1)=(0.5_WP*D)-sqrt((xm(i)-xo)**2+(ym(j)-yo)**2)
        end do
     end do
     ! Convert distance with sigmoid transform
     G(:,:,1)=1.0_WP/(1.0_WP+exp(-2.0_WP*G(:,:,1)/(epsG+epsG2)))
     ! G(:,:,1)=0.5_WP*(tanh(G(:,:,1)/(2.0_WP*epsG))+1.0_WP)

  case ('UncertainCircle','uncertaincircle','Uncertaincircle')
     ! For uncertainty about the x direction only
     call uncertain_circle

  case ('Disk','disk','DISK','ZalesaksDisk')
     call zalesaks_disk

  case ('Deformation','deformation','DEFORMATION')
     ! Create distance level set
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           G(i,j,1)=(0.15_WP)-sqrt((xm(i)-0.5_WP)**2+(ym(j)-0.75_WP)**2)
        end do
     end do
     ! Convert distance with sigmoid transform
     G(:,:,1)=1.0_WP/(1.0_WP+exp(-2.0_WP*G(:,:,1)/(epsG+epsG2))) 

  case ('Jet','jet','JET')
     ! Initialize Level Set at Jet interface
     G = 0.0_WP
     ! Create distance level set
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           G(i,j,1)=(0.05_WP*(ymax-ymin))-sqrt((xm(i)-xmin)**2+(ym(j)-0.5_WP*(ymax-ymin))**2)
        end do
     end do
     ! Do distance level set on ghost cells (creates a tube)
     if (rankx.eq.0) then
        do j=jmino_,jmaxo_
           do i=imino_,imin_-1
              if (ym(j).gt.0.5_WP*(ymax-ymin)) then
                 G(i,j,1) = (0.5_WP*(ymax-ymin)+0.05_WP*(ymax-ymin)) - ym(j)
              else
                 G(i,j,1) = ym(j) - (0.5_WP*(ymax-ymin)-0.05_WP*(ymax-ymin))
              end if
           end do
        end do
     end if
     ! Convert distance with sigmoid transform
     G(:,:,1)=1.0_WP/(1.0_WP+exp(-2.0_WP*G(:,:,1)/(epsG+epsG2)))
     ! G(:,:,1)=0.5_WP*(tanh(G(:,:,1)/(1.25_WP*(epsG+epsG2)))+1.0_WP)

  case ('CrossflowJet','crossflowjet','CROSSFLOWJET','Crossflowjet')
     ! Initialize Level Set at Jet interface
     G = 0.0_WP
     ! Create distance level set
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           G(i,j,1)=(0.05_WP*(xmax-xmin))-sqrt((xm(i)-0.5_WP*(xmax-xmin))**2+(ym(j)-ymin)**2)
        end do
     end do
     if (ranky.eq.0) then
        do j=jmino_,jmin_-1
           G(:,j,1) = G(:,jmin_,1)
        end do
     end if
     ! Convert distance with sigmoid transform
     G(:,:,1)=1.0_WP/(1.0_WP+exp(-2.0_WP*G(:,:,1)/(epsG+epsG2)))

  case ('Logo','FCL')
     call fcl_logo

  case ('UncertainDeformation')
     call uncertain_deformation

  case ('Ellipse','elipse','ellipse','ELLIPSE','ELIPSE','Elipse')
     ! Create distance level set
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           theta = atan2(ym(j)-yo,xm(i)-xo)
           xe = (1.0_WP/((1.0_WP/semimajor**2)+(tan(theta)**2)/(semiminor**2)))
           G(i,j,1) = sqrt(xe + (semiminor**2)*(1.0_WP-xe/(semimajor**2))) - &
                sqrt((xm(i)-xo)**2+(ym(j)-yo)**2)
        end do
     end do
     ! Convert distance to hyperbolic tangent
     ! G(:,:,1)=1.0_WP/(1.0_WP+exp(-1.125_WP*G(:,:,1)/(epsG+epsG2)))
     G(:,:,1)=1.0_WP/(1.0_WP+exp(-2.0_WP*G(:,:,1)/(epsG+epsG2)))

  case ('Pool','POOL','pool')
     ! Create distance level set
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           G(i,j,1)=(0.5_WP*D)-sqrt((xm(i)-xo)**2+(ym(j)-yo)**2)
        end do
     end do
     ! Convert distance with sigmoid transform
     G(:,:,1) = 1.0_WP/(1.0_WP+exp(-2.0_WP*G(:,:,1)/(epsG+epsG2)))

     ! Add pool of water at bottom
     do j = jmino_,jmaxo_
        if (ym(j).lt.1.5_WP) then
           G(:,j,1) = 1.0_WP
        end if
     end do

  case default
     call die('Unknown level set shape: '//trim(Gshape))
  end select

  ! Communicate
  call MPI_BARRIER(COMM, verr)
  call comm_borders(G)

  return
end subroutine geometry_init

!=========================!
!     Zalesak's Disk      !
!=========================!
subroutine zalesaks_disk
  use geometry
  implicit none
  integer :: i,j,br
  real(WP) :: Nw,Nh

  ! Center the disk
  xo = 0.0_WP
  yo = 0.25_WP
  D  = 0.4_WP

  ! Set Disk Attributes
  Nw = 0.2_WP; Nh = 0.65_WP
  br = floor(Nw*0.5_WP*D/max(dx,dy))-4

  ! Form the disk
  do j=jmin_,jmax_
     do i=imin_,imax_
        !  Map a circle
        G(i,j,1)=(0.5_WP*D)-sqrt((xm(i)-xo)**2+(ym(j)-yo)**2)
        !  Form a Notch
        if ((abs(xm(i)-xo).lt.(Nw*0.5_WP*D)).and.((ym(j)-yo).lt.(Nh*0.5_WP*D)).and.(G(i,j,1).gt.0.0_WP)) then
           G(i,j,1) = -1.0_WP
        end if
     end do
  end do

  ! Communicate
  call comm_borders(G)

  ! Transform with a Sigmoid
  G(:,:,1)=1.0_WP/(1.0_WP+exp(-2.0_WP*G(:,:,1)/(epsG+epsG2)))

  return
end subroutine zalesaks_disk

!================================!
!     Uncertain Deformation      !
!================================!
subroutine uncertain_deformation
  use geometry
  implicit none
  integer :: i,j,k
  real(WP), dimension(1:ndof) :: xok, yok

  ! Uncertainty about the x location
  ! Set expected values in first location
  xok = 0.0_WP; xok(1) = 0.5_WP
  xok(2) = 0.05_WP
  yok = 0.0_WP; yok(1) = 0.75_WP

  ! Compute the transformed weights (Psi)
  do j = jmin_,jmax_
     do i = imin_,imax_
        do k = 1,ndof
           G(i,j,k) = UCcircle_romberg(xok,yok,phi(k-1),xm(i), &
                ym(j),0.15_WP,(epsG+epsG2),zetamin,zetamax)/Var(k)
        end do
     end do
  end do

  ! Communicate
  call comm_borders(G)

  return
end subroutine uncertain_deformation

!===========================!
!     Uncertain Circle      !
!===========================!
subroutine uncertain_circle
  use geometry
  implicit none
  integer :: i,j,k
  real(WP), dimension(1:ndof) :: xok, yok

  ! Uncertainty about the x and y location
  ! Set expected values in first location
  xok = 0.0_WP; xok(1) = xo
  xok(2) = sigmaI
  yok = 0.0_WP; yok(1) = yo
  ! yok(2) = 0.05_WP

  ! Compute the transformed weights (Psi)
  do j = jmin_,jmax_
     do i = imin_,imax_
        do k = 1,ndof
           G(i,j,k) = UCcircle_romberg(xok,yok,phi(k-1),xm(i),ym(j), &
                0.5_WP*D,(epsG+epsG2),-1.0_WP,1.0_WP)/Var(k)
        end do
     end do
  end do

  ! Communicate
  call comm_borders(G)

  return
end subroutine uncertain_circle

!====================================!
!        FCL Logo Development        !
!====================================!
subroutine fcl_logo
  use geometry
  implicit none
  integer :: i,j,xw,yw
  real(WP) :: HL,LH,nxx,nyy,crr,theta1
  integer :: iimax, iimin, Fw, Fd, Lw, xcs

  ! Allocate level set
  allocate(FFF    (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(CCC    (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(LLL    (imino_:imaxo_,jmino_:jmaxo_,1:ndof))

  ! Request inputs
  call read_input('nx',nxx)
  call read_input('ny',nyy)

  ! Create Logo Level Set
  ! Create Block Lettering
  ! Letter width
  xw = ceiling(nxx/50)
  yw = ceiling(nyy/50)

  ! ! Create the letter F
  FFF = 0.0_WP
  FFF(ceiling(nxx/3):ceiling(nxx/3)+xw,ceiling(nyy/3):ceiling(2*nyy/3),1) = 1.0_WP
  FFF(ceiling(nxx/3)+xw:ceiling(nxx*0.48)+xw,ceiling(2*nyy/3)-yw:ceiling(2*nyy/3),1) = 1.0_WP
  FFF(ceiling(nxx/3)+xw:ceiling(nxx*0.42)+xw,ceiling(nyy*0.53)-yw:ceiling(nyy*0.53),1) = 1.0_WP

  ! Create the letter C
  HL = 1.0_WP; LH = 1.0_WP
  iimax = imin_; iimin = imax_
  crr = 0.159_WP ! radius (0.159)
  theta1 = pi/4.0_WP
  CCC = 0.0_WP
  do i = imin_,imax_
     do j = jmin_,jmax_
        if ((abs(sqrt((x(i)-0.5_WP*LH)**2 + (y(j)-0.5_WP*HL)**2) - crr).lt.0.01_WP).and. &
             (atan2(abs(y(j)-0.5_WP*HL),x(i)-0.5_WP*LH).gt.theta1)) then
           CCC(i,j,1) = 1.0_WP
           iimax = max(iimax,i)
           iimin = min(iimin,i)
        end if
     end do
  end do

  ! Create the letter L
  LLL = 0.0_WP
  LLL(ceiling(nxx/3):ceiling(nxx/3)+xw,ceiling(nyy/3):ceiling(2*nyy/3),1) = 1.0_WP
  LLL(ceiling(nxx/3)+xw:ceiling(nxx*0.48)+xw,ceiling(nyy/3):ceiling(nyy/3)+yw,1) = 1.0_WP

  ! Shift and Combine
  Fw = (ceiling(nxx*0.48)+xw)-ceiling(nxx/3)
  Lw = (ceiling(nxx*0.48)+xw)-ceiling(nxx/3)
  Fd = ceiling(nxx*0.48) - ceiling(nxx*0.42)
  xcs = ceiling(real(Fd,WP)/2.0_WP)
  G((iimin-xcs-Fw+Fd):(iimin-xcs+Fd),:,1) = FFF(ceiling(nxx/3):ceiling(nxx*0.48)+xw,:,1)
  G = G + CCC
  G((iimax+xcs):(iimax+xcs+Lw),:,1) = LLL(ceiling(nxx/3):ceiling(nxx*0.48)+xw,:,1)

  ! Shift everything vertically and rescale
  G=2.0_WP*G; G=G-1.0_WP; G=0.5_WP*G;

  ! Transform with a Sigmoid
  epsG = 2.0_WP*max(dx,dy)
  G(:,:,1)=1.0_WP/(1.0_WP+exp(-G(:,:,1)/(0.5_WP*epsG)))

  ! Communicate   
  call comm_borders(G)

  return
end subroutine fcl_logo

!===========================================!
!       Level set boundary conditions       !
!===========================================!
subroutine geometry_bc
  use geometry
  implicit none

  integer :: kk
  integer :: verr

  ! Update boundary conditions based on test case
  select case(trim(simu))

  case ('null','Null','NULL')
     ! Deploy Neumann condition on domain
     do kk=1,nghost
        if (rankx.eq.0   ) G(imin_-kk,:,:) = G(imin_,:,:)
        if (rankx.eq.px-1) G(imax_+kk,:,:) = G(imax_,:,:)
        if (ranky.eq.0   ) G(:,jmin_-kk,:) = G(:,jmin_,:)
        if (ranky.eq.py-1) G(:,jmax_+kk,:) = G(:,jmax_,:)
     end do

  case ('Jet','JET','jet')
     ! Deploy Neumann condition on domain
     do kk=1,nghost
        if (rankx.eq.px-1) G(imax_+kk,:,:) = G(imax_,:,:)
        if (ranky.eq.0   ) G(:,jmin_-kk,:) = G(:,jmin_,:)
        if (ranky.eq.py-1) G(:,jmax_+kk,:) = G(:,jmax_,:)
     end do
     ! Incoming jet profile should be constant
     if (rankx.eq.0)  G(imino_:imin_-1,:,:) = Gsave(imino_:imin_-1,:,:)

  case ('UCJet','UCJET','ucjet','UCjet')
     ! Deploy Neumann condition on domain
     do kk=1,nghost
        if (rankx.eq.px-1) G(imax_+kk,:,:) = G(imax_,:,:)
        if (ranky.eq.0   ) G(:,jmin_-kk,:) = G(:,jmin_,:)
        if (ranky.eq.py-1) G(:,jmax_+kk,:) = G(:,jmax_,:)
     end do
     ! Incoming jet profile should be constant
     if (rankx.eq.0) G(imino_:imin_-1,:,:) = Gsave(imino_:imin_-1,:,:)

  case ('CrossflowJet','CROSSFLOWJET','crossflowjet','Crossflowjet')
     do kk=1,nghost
        ! Incoming fluid is external
        if (rankx.eq.0   ) G(imin_-kk,:,:) = 0.0_WP
        ! Neumann conditions on remainder
        if (rankx.eq.px-1) G(imax_+kk,:,:) = G(imax_,:,:)
        if (ranky.eq.py-1) G(:,jmax_+kk,:) = G(:,jmax_,:)
     end do
     ! Incoming jet profile should be constant
     if (ranky.eq.0)  G(:,jmino_:jmin_,:) = Gsave(:,jmino_:jmin_,:)

  case ('uncertainvelocity','UncertainVelocity','UNCERTAINVELOCITY')
     ! Set outside to be external fluid
     if (rankx.eq.0   ) G(imino_:imin_-1,:,:) = 0.0_WP
     if (rankx.eq.px-1) G(imax_+1:imaxo_,:,:) = 0.0_WP
     if (ranky.eq.0   ) G(:,jmino_:jmin_-1,:) = 0.0_WP
     if (ranky.eq.py-1) G(:,jmax_+1:jmaxo_,:) = 0.0_WP

  case ('Deformation','deformation','UncertainDeformation')
     ! Deploy Neumann condition on domain
     do kk=1,nghost
        if (rankx.eq.0   ) G(imin_-kk,:,:) = G(imin_,:,:)
        if (rankx.eq.px-1) G(imax_+kk,:,:) = G(imax_,:,:)
        if (ranky.eq.0   ) G(:,jmin_-kk,:) = G(:,jmin_,:)
        if (ranky.eq.py-1) G(:,jmax_+kk,:) = G(:,jmax_,:)
     end do

  case ('Disk','disk','DISK','ZalesaksDisk','UCZalesaksDisk','UCDisk','UCdisk','UCDISK')
     ! Deploy Neumann condition on domain
     do kk=1,nghost
        if (rankx.eq.0   ) G(imin_-kk,:,:) = G(imin_,:,:)
        if (rankx.eq.px-1) G(imax_+kk,:,:) = G(imax_,:,:)
        if (ranky.eq.0   ) G(:,jmin_-kk,:) = G(:,jmin_,:)
        if (ranky.eq.py-1) G(:,jmax_+kk,:) = G(:,jmax_,:)
     end do

  case default
     call die('Unknown simulation (geometry.f90): '//trim(simu))
  end select

  ! Hold until all processors are done
  call MPI_BARRIER(COMM, verr)

  return
end subroutine geometry_bc
