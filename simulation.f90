! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module simulation
  use communication
  use variables
  use pressure
  use velocity
  use boundary
  use levelset
  use basis
  use grid
  use math
  use io
  implicit none

  ! Dummy module

end module simulation

!===========================!
!      Initialization       !
!===========================!
subroutine simulation_init
  use simulation
  implicit none

  integer :: i, j, verr
  real(WP), parameter :: U_o=0.05_WP
  real(WP), parameter :: U_p=0.005_WP

  ! Initialize mask
  ! mask=1. : Normal cell
  ! mask=0. : Wall 
  allocate(mask(imino_:imaxo_,jmino_:jmaxo_))
  allocate(Cz(1:ndof))
  mask=1.0_WP

  ! Initialize velocity field
  select case(trim(simu))

  case ('poiseuile')
     u=0.0_WP
     u(:,:,1)=1.0_WP
     if (ranky.eq.0)    u(:,jmin-1,1)=-1.0_WP
     if (ranky.eq.py-1) u(:,jmax+1,1)=-1.0_WP
     v=0.0_WP

  case ('Couette','couette','COUETTE')
     u = 0.0_WP
     v = 0.0_WP
     if (ranky.eq.0) u(:,jmin_-1,1) = 1.0_WP

  case ('mixing-layer')
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! U velocity
           u(i,j,:)=U_o*tanh(20*ym(j)/(ymax-ymin))+U_p*sin(pi/2*(xmax-xmin)*x(i))
        end do
     end do
     u(:,jmax+1,:)=+U_o
     u(:,jmin-1,:)=-U_o
     u(imax+1,:,:)=u(imin,:,:)
     u(imin-1,:,:)=u(imax,:,:)

     ! V velocity
     v=0.0_WP

  case ('cylinder')
     u=0.0_WP
     v=0.0_WP
     u(:,:,1)=3.0d-2
     do j=jmino_,jmaxo_
        u(:,j,1)=u(:,j,1)+cos(2*pi*y(j))*u(:,j,1)*0.1_WP
        do i=imino_,imaxo_
           if (sqrt(x(i)**2+ym(j)**2).lt.0.5_WP) then
              u(i,j,1)=0.0_WP
              mask(i,j)=0.0_WP
           end if
        end do
     end do
     if (rankx.eq.0) u(imin,:,1)=3.0d-2

  case ('square')
     u=0.0_WP
     v=0.0_WP
     u(:,:,1)=3.0d-2
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (abs(x(i)).le.0.25_WP.and.abs(ym(j)).le.0.5_WP) then
              u(i,j,1)=0.0_WP
              mask(i,j)=0.0_WP
           end if
        end do
     end do

  case ('uncertainvelocity', 'UncertainVelocity', 'UCvelocity', 'ucvelocity')
     u = 0.0_WP; v = 0.0_WP
     u(:,:,1) = 1.75_WP
     u(:,:,2) = 0.25_WP

  case ('channel')
     u = 0.0_WP; v = 0.0_WP
     u(:,:,1) = 2.0_WP

  case ('Null','None','NULL','NONE','null','none')
     u = 0.0_WP; v = 0.0_WP

  case ('Deformation','UncertainDeformation','uncertaindeformation','deformation')
     ! Velocity field at a given time
     call velocity_deformation

  case ('ZalesaksDisk','Disk','disk','DISK')
     ! Set magnitude scalar
     Cz = 0.0_WP; Cz(1) = 2.0_WP

     ! Zalesak's Disk Velocity Field
     u = 0.0_WP; v = 0.0_WP
     do i=imino_,imaxo_
        do j=jmino_,jmaxo_
           u(i,j,1) = -Cz(1)*Pi*ym(j)
           v(i,j,1) = +Cz(1)*Pi*xm(i)
        end do
     end do

  case ('UCZalesaksDisk','UCDisk','UCdisk','UCDISK')
     ! Set magnitude scalars
     Cz = 0.0_WP; Cz(1) = 2.00_WP; Cz(2) = 0.02_WP

     ! Uncertain Zalesak's Disk Velocity Field
     u = 0.0_WP; v = 0.0_WP
     do i=imino_,imaxo_
        do j=jmino_,jmaxo_
           u(i,j,1) = -Cz(1)*Pi*ym(j)
           v(i,j,1) = +Cz(1)*Pi*xm(i)
           u(i,j,2) = -Cz(2)*Pi*ym(j)
           v(i,j,2) = +Cz(2)*Pi*xm(i)
        end do
     end do

  case ('OscillatingDroplet','oscillatingdroplet','OSCILLATINGDROPLET','Droplet','droplet')
     u = 0.0_WP; v = 0.0_WP

  case ('CapFlow','capflow','Cap Flow','Capflow')
     u = 0.0_WP; v = 0.0_WP
     if (ranky.eq.py-1) then
        do i = 1,nghost
           u(:,jmax_+i,:)=1.0_WP
        end do
     end if

  case ('Jet','jet','JET')
     ! Set initial velocity as a function of levelset
     u = 0.0_WP; v = 0.0_WP
     where (G(:,:,1).gt.0.5_WP) u(:,:,1) = Umag

  case ('UCJet','UCjet','UCJET','ucjet')
     ! Set initial velocity as a function of levelset
     u = 0.0_WP; v = 0.0_WP
     where (G(:,:,1).gt.0.5_WP) u(:,:,1) = Umag
     where (G(:,:,1).gt.0.5_WP) u(:,:,2) = Umag_var

  case ('CrossflowJet','crossflowjet','CROSSFLOWJET','Crossflowjet')
     u = 0.0_WP
     v = 0.0_WP
     if (ranky.eq.0) then
        ! Incoming Jet
        do i = imino_,imaxo_
           if ((xm(i).lt.(0.5_WP*(xmax-xmin)+0.05_WP*(xmax-xmin))).and. &
                (xm(i).gt.(0.5_WP*(xmax-xmin)-0.05_WP*(xmax-xmin)))) then
              v(i,jmino_:jmin_,1) = 100.0_WP
           end if
        end do
     end if
     if (rankx.eq.0) u(imino_:imin_,:,1) = 50.0_WP

  case default
     call die('Unknown simulation (simulation.f90): '//trim(simu))
  end select

  ! Form masks on edges of domain
  if (.not.isxper) then
     if (rankx.eq.0)    mask(imin-1,:)=0.0_WP
     if (rankx.eq.px-1) mask(imax+1,:)=0.0_WP
  end if
  if (.not.isyper) then
     if (ranky.eq.0)    mask(:,jmin-1)=0.0_WP
     if (ranky.eq.py-1) mask(:,jmax+1)=0.0_WP
  end if

  ! Communicate
  call MPI_BARRIER(COMM, verr)
  call comm_borders(u); call comm_borders(v)

  return
end subroutine simulation_init

!===================================!
!          Solution Update          !
!===================================!
subroutine simulation_step
  use simulation
  implicit none
  real(WP) :: myRes, Res
  integer :: i

  ! Save current information & estimate P_hat
  u_now = u; v_now = v; G_now = G
  ! if (.not.testPr) Pr = 2.0_WP*Pr_now - Pr_old
  ! if (.not.testPr) Pr = 0.0_WP
  if (.not.testPr) call pressure_phat

  ! Compute new timestep for transport
  call simulation_dt

  ! Implement Crank-Nicolson
  ctr1 = 0; Res = 10.0_WP
  u_old = 0.0_WP; v_old = 0.0_WP; Gold = 0.0_WP
  do while ((Res.gt.Vconverge).and.(ctr1.lt.cni))
     ctr1 = ctr1 + 1

     ! Calculate half-step information
     u = 0.5_WP*(u_now + u); call comm_borders(u)
     v = 0.5_WP*(v_now + v); call comm_borders(v)
     G = 0.5_WP*(G_now + G); call comm_borders(G)
     call levelset_rhs

     if (.not.LST) then
        ! Calculate the curvature
        call levelset_curvature

        ! Calculate predictor step
        call timing_pause(3)
        call timing_resume(1)
        call velocity_rhs
        call velocity_source
        ustar = u_now + dt*(rhsU + sourceU)
        vstar = v_now + dt*(rhsV + sourceV)

        ! Communicate
        call comm_borders(ustar)
        call comm_borders(vstar)

        ! Apply boundary conditions
        call velocity_bc
        call timing_pause(1)
        call timing_resume(3)

        ! Find pressure field
        call timing_pause(3)
        call timing_resume(2)
        if (.not.testPr) then
           ! ! Fast Pressure Testing
           ! P_save = Pr
           ! call pressure_test
           ! P_other = Pr
           ! Pr = P_save

           ! Run fast pressure solver
           call pressure_fast
           ! P_diff = abs(P_hat - Pr)
           ! P_diff = P_other - Pr
        else
           ! Run standard solver
           call pressure_test
        end if
        call timing_pause(2)
        call timing_resume(3)

        ! Correct velocity
        call timing_pause(3)
        call timing_resume(1)
        call velocity_correct
        ! call velocity_standard ! Fast pressure testing
        call timing_pause(1)
        call timing_resume(3)

     else
        ! Level set test case velocities
        call simulation_init
     end if

     ! Update level set
     G = G_now + dt*rhsG
     call comm_borders(G)
     call geometry_bc

     ! Update convergence
     myRes = max(max(maxval(abs(u-u_old)),maxval(abs(v-v_old))),maxval(abs(G-Gold)))
     call max_real(myRes,Res)
     u_old = u; v_old = v; Gold = G
  end do

  ! Save pressure for next iteration
  if (.not.testPr) then
     Pr_old = Pr_now
     Pr_now = Pr
  end if

  ! Reinitialize the level set
  call simulation_dtau
  do i = 1,mrs
     call levelset_reinit
  end do

  ! Calculate Mass and Volume
  call data_volume

  ! Calculate Probability
  if (LSProbCalc) call levelset_probability

  ! Compute divergence
  call velocity_divergence

  ! Compute Kinetic Energy
  call KineticEnergy

  return
end subroutine simulation_step

!======================================!
!          Calculate Time Step         !
!======================================!
subroutine simulation_dt
  use simulation
  implicit none
  integer  :: i
  real(WP) :: umax, vmax
  real(WP) :: rhomin, stmax
  real(WP) :: zstep, dt_local
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: u_point, v_point

  ! Calculate zeta window width
  zstep = (zetamax-zetamin)/100.0_WP

  ! Find max velocity of several possibilities (101 sample points)
  umax = 0.0_WP; vmax = 0.0_WP; rhomin = 10.0_WP**5; stmax = 0.0_WP
  do i = 0,100
     u_point = abs(poly_eval2(u,(zetamin+zstep*real(i,WP))))
     v_point = abs(poly_eval2(v,(zetamin+zstep*real(i,WP))))
     umax = max(umax, maxval(u_point(imin_:imax_+1,jmin_:jmax_)))
     vmax = max(vmax, maxval(v_point(imin_:imax_,jmin_:jmax_+1)))
     rhomin = min(rhomin, abs(real(poly_eval(rhoE+rhoI,(zetamin+zstep*real(i,WP))),WP)))
     stmax = max(stmax, abs(real(poly_eval(gammak,(zetamin+zstep*real(i,WP))),WP)))
  end do

  ! Compute timestep for transport
  dt_local = CFL*min(min(dx,dy)/max(umax,vmax), min((min(dx,dy)**2)/(2.0_WP*maxval(nu+epsilon(0.0_WP))), &
       sqrt(rhomin/(4.0_WP*pi*stmax))*(min(dx,dy)**1.5_WP)))
  call min_real(dt_local,dt)
  dt = min(dt,dtmin)

  return
end subroutine simulation_dt

!=============================================!
!          Calculate Psuedo-time Step         !
!=============================================!
subroutine simulation_dtau
  use simulation
  implicit none
  real(WP) :: dTau
  real(WP) :: dtre_local
  real(WP) :: scale_local, scale

  ! Calculate normal vectors
  call levelset_normal

  ! Calculate pseudotime step
  dtre_local = reCFL*min(dx,dy)/(3.0_WP*max(maxval(abs(Nox)),maxval(abs(Noy))))
  call min_real(dtre_local,dtre)
  dtre = min(dtre,reCFL*min(min(dx,dy),min(dx**2,dy**2)/(4.0_WP*(epsG+epsG2))))
  ! dtre = reCFL*min(min(dx,dy),min(dx**2,dy**2)/(4.0_WP*(epsG+epsG2)))

  ! Find velocities at cell center
  Uint = 0.0_WP; Vint = 0.0_WP
  Uint(imin_:imax_,:,:) = 0.5_WP*(u(imin_+1:imax_+1,:,:) + u(imin_:imax_,:,:))
  Vint(:,jmin_:jmax_,:) = 0.5_WP*(v(:,jmin_+1:jmax_+1,:) + v(:,jmin_:jmax_,:))
  call comm_borders(Uint); call comm_borders(Vint)

  ! Find reinitialization steps and scale
  scale_local = maxval(sqrt((Nox(:,:,1)*Uint(:,:,1))**2 + (Noy(:,:,1)*Vint(:,:,1))**2))
  call max_real(scale_local,scale)
  dTau = F_tau*dt*scale
  mrs = ceiling(dTau/dtre)
  dtre = dTau/mrs

  return
end subroutine simulation_dtau


