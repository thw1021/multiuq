! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module visit
  use parallel
  use variables
  use grid
  use basis
  use math
  use iso_c_binding
  
  implicit none
  include 'silo_f9x.inc'

  interface
     function mkdir(path,mode) bind(c,name="mkdir")
       use iso_c_binding
       integer(c_int) :: mkdir
       character(kind=c_char,len=1) :: path(*)
       integer(c_int16_t), value :: mode
     end function mkdir
  end interface
  
  ! Time info
  integer :: nout_time

  ! Write count
  integer :: visit_count
  integer :: visit_freq
  
  ! Output variables
  integer :: output_n,output_max
  character(len=str_medium), dimension(:),   allocatable :: output_types
  character(len=str_medium), dimension(:),   allocatable :: output_names
  character(len=str_short),  dimension(:,:), allocatable :: output_compNames
  
  ! Silo database
  integer :: silo_comm,silo_rank,Nproc_node
  integer, dimension(:), allocatable :: group_ids,proc_nums
  real(WP), dimension(:,:),   allocatable ::  spatial_extents, myspatial_extents
  real(WP), dimension(:,:),   allocatable :: Uspatial_extents,Umyspatial_extents
  real(WP), dimension(:,:),   allocatable :: Vspatial_extents,Vmyspatial_extents
  real(WP), dimension(:,:,:,:), allocatable :: data_extents,   mydata_extents
  character(len=60), dimension(:), allocatable :: names
  integer, dimension(:), allocatable :: lnames,types
  
  ! Mesh 
  integer, parameter :: nGhostVisit=1
  integer :: nxout,nyout,nzout
  integer :: noverxmin_out,noverymin_out
  integer :: noverxmax_out,noverymax_out
  integer :: imin_out,jmin_out
  integer :: imax_out,jmax_out
  integer, dimension(2) :: lo_offset, hi_offset
  
  ! Buffers
  real(SP), dimension(:,:,:), allocatable :: out_buf
  
contains
  
  ! ======================================= !
  !  Data to write to file                  !
  !   - Add data to write here              !
  !   - Include name of case in input file  !
  !      e.g. Visit outputs : Velocity      !
  ! ======================================= !
  subroutine visit_getData(n)
    implicit none
    integer, intent(in) :: n
    integer :: i,j,nn
    
    select case(adjustl(trim(output_names(n))))

    !===== Velocity Field =====!
    case ('Velocity') 
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'U', 'V' /)
       do j=jmino_,jmaxo_-1
          do i=imino_,imaxo_-1
             out_buf(i,j,1)=real((u(i,j,1)+u(i+1,j  ,1))*0.5_WP,SP)
             out_buf(i,j,2)=real((v(i,j,1)+v(i  ,j+1,1))*0.5_WP,SP)
          end do
       end do

     case ('Velstar') 
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'Ustar', 'Vstar' /)
       do j=jmino_,jmaxo_-1
          do i=imino_,imaxo_-1
             out_buf(i,j,1)=real((ustar(i,j,1)+ustar(i+1,j  ,1))*0.5_WP,SP)
             out_buf(i,j,2)=real((vstar(i,j,1)+vstar(i  ,j+1,1))*0.5_WP,SP)
          end do
       end do

    case ('Source') 
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'sourceU', 'sourceV' /)
       do j=jmino_,jmaxo_-1
          do i=imino_,imaxo_-1
             out_buf(i,j,1)=real((sourceU(i,j,1)+sourceU(i+1,j  ,1))*0.5_WP,SP)
             out_buf(i,j,2)=real((sourceV(i,j,1)+sourceV(i  ,j+1,1))*0.5_WP,SP)
          end do
       end do

    case ('Vel_Var')
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'varU', 'varV' /)
       ! Second moment
       out_buf=0.0_WP
       do nn=2,ndof
          do j=jmino_,jmaxo_-1
             do i=imino_,imaxo_-1
                out_buf(i,j,1)=out_buf(i,j,1)+real((((u(i,j,nn)+u(i+1,j  ,nn))*0.5_WP)**2)*Var(nn),SP)
                out_buf(i,j,2)=out_buf(i,j,2)+real((((v(i,j,nn)+v(i  ,j+1,nn))*0.5_WP)**2)*Var(nn),SP)
             end do
          end do
       end do

    case ('Divergence') 
       output_types(n)='scalar'
       out_buf = 0.0_WP
       out_buf(:,:,1)=real(poly_eval2(div_u,0.0_WP),SP)

    case ('Vel_diff') 
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'u_diff', 'v_diff' /)
       do j=jmino_,jmaxo_-1
          do i=imino_,imaxo_-1
             out_buf(i,j,1)=real((u_diff(i,j,1)+u_diff(i+1,j  ,1))*0.5_WP,SP)
             out_buf(i,j,2)=real((v_diff(i,j,1)+v_diff(i  ,j+1,1))*0.5_WP,SP)
          end do
       end do

    !===== Viscosity Outputs =====!

    case ('Viscosity') 
       output_types(n)='scalar'
       out_buf = 0.0_WP
       out_buf(:,:,1)=real(poly_eval2(nu,0.0_WP),SP)

    case ('nu_Var')
       output_types(n)='scalar'
       ! Second moment
       out_buf=0.0_WP
       do nn=2,ndof
          out_buf(:,:,1)=out_buf(:,:,1)+real((nu(:,:,nn)**2)*Var(nn),SP)
       end do

    !===== Pressure Outputs =====!
    case ('Pressure')
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(:,:,1)=real(poly_eval2(Pr,0.0_WP),SP)

    case ('P0')
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(:,:,1)=real(Pr(:,:,1),SP)
       
    case ('P1')
       output_types(n)='scalar'
       out_buf=0.0_WP
       if (ndof.gt.1) then
          out_buf(:,:,1)=real(Pr(:,:,2),SP)
       end if

     case ('P2')
       output_types(n)='scalar'
       out_buf=0.0_WP
       if (ndof.gt.2) then
          out_buf(:,:,1)=real(Pr(:,:,3),SP)
       end if

    case ('P_Var')
       output_types(n)='scalar'
       ! Second moment
       out_buf=0.0_WP
       do nn=2,ndof
          out_buf(:,:,1)=out_buf(:,:,1)+real((Pr(:,:,nn)**2)*Var(nn),SP)
       end do

    case ('P_standard')
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(:,:,1)=real(poly_eval2(P_other,0.0_WP),SP)

    case ('P_diff')
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(:,:,1)=real(poly_eval2(P_diff,0.0_WP),SP)

    !===== Kinetic Energy Outputs =====!
    case ('KineticEnergy')
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(:,:,1)=real(KE(:,:,1),SP)

    case ('KE_Var')
       output_types(n)='scalar'
       ! Second moment
       out_buf=0.0_WP
       do nn=2,ndof
          out_buf(:,:,1)=out_buf(:,:,1)+real((KE(:,:,nn)**2)*Var(nn),SP)
       end do

    !===== Level set outputs =====!
    case ('G0')
       output_types(n)='scalar'
       out_buf=0.0_WP
       ! Output average level set value
       out_buf(:,:,1)=real(G(:,:,1),SP)

    case ('G1')
       output_types(n)='scalar'
       out_buf=0.0_WP
       ! Output 2nd basis weight
       if (ndof.gt.1) then
          out_buf(:,:,1)=real(G(:,:,2),SP)
       end if

    case ('G2')
       output_types(n)='scalar'
       out_buf=0.0_WP
       ! Output 3rd basis weight
       if (ndof.gt.2) then
          out_buf(:,:,1)=real(G(:,:,3),SP)
       end if

    case ('G3')
       output_types(n)='scalar'
       out_buf=0.0_WP
       ! Output 3rd basis weight
       if (ndof.gt.3) then
          out_buf(:,:,1)=real(G(:,:,4),SP)
       end if

    case ('G_Var')
       output_types(n)='scalar'
       ! Second moment
       out_buf=0.0_WP
       do nn=2,ndof
          out_buf(:,:,1)=out_buf(:,:,1)+real((G(:,:,nn)**2)*Var(nn),SP)
       end do

    case ('Gzm1')
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(:,:,1) = real(poly_eval2(G,-1.0_WP),SP)
       
    case ('Gz0')
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(:,:,1) = real(poly_eval2(G,0.0_WP),SP)

    case ('Gzp1')
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(:,:,1) = real(poly_eval2(G,+1.0_WP),SP)

    case ('Normal')
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'Nx', 'Ny' /)
       out_buf=0.0_WP
       out_buf(:,:,1) = real(poly_eval2(Nox,0.0_WP),SP)
       out_buf(:,:,2) = real(poly_eval2(Noy,0.0_WP),SP)

    case ('Gsave')
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(:,:,1)=real(poly_eval2(Gsave,0.0_WP),SP)

    case ('Gprob')
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(:,:,1)=real(Gprob(:,:,1),SP)

    case ('Ganalytic')
       output_types(n) = 'scalar'
       out_buf=0.0_WP
       if ((trim(simu).eq.'uncertainvelocity').or.(trim(simu).eq.'channel')) then
          ! Create distance level set (for zeta = 0)
          do j=jmino_,jmaxo_
             do i=imino_,imaxo_
                out_buf(i,j,1)=real((0.5_WP*D)-sqrt((xm(i)-(xo+maxval(u(:,:,1))*time))**2+(ym(j)-yo)**2),SP)
             end do
          end do
          ! Convert distance with a sigmoid transform
          out_buf(:,:,1)=real(1.0_WP/(1.0_WP+exp(-out_buf(:,:,1)/max(dx,dy))),SP)
       end if

    case ('GanalyticM1')
       output_types(n) = 'scalar'
       out_buf=0.0_WP
       if (trim(simu).eq.'uncertainvelocity') then
          ! Create distance level set (for zeta = 0)
          do j=jmino_,jmaxo_
             do i=imino_,imaxo_
                out_buf(i,j,1)=real((0.5_WP*D)-sqrt((xm(i)-(xo+maxval(u(:,:,1)-u(:,:,2))*time))**2+(ym(j)-yo)**2),SP)
             end do
          end do
          ! Convert distance with a sigmoid transform
          out_buf(:,:,1)=real(1.0_WP/(1.0_WP+exp(-out_buf(:,:,1)/max(dx,dy))),SP)
       end if

    case ('GanalyticP1')
       output_types(n) = 'scalar'
       out_buf=0.0_WP
       if (trim(simu).eq.'uncertainvelocity') then
          ! Create distance level set (for zeta = 0)
          do j=jmino_,jmaxo_
             do i=imino_,imaxo_
                out_buf(i,j,1)=real((0.5_WP*D)-sqrt((xm(i)-(xo+maxval(u(:,:,1)+u(:,:,2))*time))**2+(ym(j)-yo)**2),SP)
             end do
          end do
          ! Convert distance with a sigmoid transform
          out_buf(:,:,1)=real(1.0_WP/(1.0_WP+exp(-out_buf(:,:,1)/max(dx,dy))),SP)
       end if

    !============== Surface Tension ==============!

    case ('SurfTens') 
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'Stx', 'Sty' /)
       out_buf=0.0_WP
       do j=jmino_,jmaxo_-1
          do i=imino_,imaxo_-1
             out_buf(i,j,1)=real((Kx(i,j,1)+Kx(i+1,j  ,1))*0.5_WP,SP)
             out_buf(i,j,2)=real((Ky(i,j,1)+Ky(i  ,j+1,1))*0.5_WP,SP)
          end do
       end do
       
    case ('sTensionz0')
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'Kx0', 'Ky0' /)
       out_buf=0.0_WP
       out_buf(:,:,1) = real(poly_eval2(Kx,0.0_WP),SP)
       out_buf(:,:,2) = real(poly_eval2(Ky,0.0_WP),SP)

    case ('sTensionzM1')
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'KxM1', 'KyM1' /)
       out_buf=0.0_WP
       out_buf(:,:,1) = real(poly_eval2(Kx,-1.0_WP),SP)
       out_buf(:,:,2) = real(poly_eval2(Ky,-1.0_WP),SP)

    case ('sTensionzP1')
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'KxP1', 'KyP1' /)
       out_buf=0.0_WP
       out_buf(:,:,1) = real(poly_eval2(Kx,1.0_WP),SP)
       out_buf(:,:,2) = real(poly_eval2(Ky,1.0_WP),SP)

    case ('DivN')
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(:,:,1)=real(Ks(:,:,1),SP)

    !===== Zalasak's Disk =====!
    case ('Zanalytic')
       output_types(n) = 'scalar'
       out_buf=0.0_WP
       if ((trim(simu).eq.'Disk').or.(trim(simu).eq.'UCDisk')) then
          if (time.gt.0.0_WP) then
             out_buf(:,:,:)=real(Zalesaks(-Cz(1)*Pi*yo*time,Cz(1)*Pi*xo*time),SP)
          else
             out_buf(:,:,1) = real(Gsave(:,:,1),SP)
          end if
       end if

       
    !===== Curvature Outputs =====!
    case ('Ks')
       output_types(n)='scalar'
       out_buf=0.0_WP
       ! Output average curvature value
       out_buf(:,:,1)=real(Ks(:,:,1),SP)

    case ('Kz0')
       output_types(n)='scalar'
       out_buf=0.0_WP
       out_buf(:,:,1) = real(poly_eval2(Ks,0.0_WP),SP)

    case ('K_Var')
       output_types(n)='scalar'
       ! Second moment
       out_buf=0.0_WP
       do nn=2,ndof
          do j=jmino_,jmaxo_
             do i=imino_,imaxo_
                out_buf(i,j,1)=out_buf(i,j,1)+real((Ks(i,j,nn)**2)*Var(nn),SP)
             end do
          end do
       end do

    !!!!! Density Outputs !!!!!
    case ('Density')
       output_types(n)='scalar'
       out_buf=0.0_WP
       ! Output density at zeta = 0
       out_buf(:,:,1)=real(poly_eval2(rhok,0.0_WP),SP)

    case ('Density2')
       output_types(n)='scalar'
       out_buf=0.0_WP
       ! Output 2nd density weight
       if (ndof.gt.1) then
          out_buf(:,:,1)=real(rhok(:,:,2),SP)
       end if

    case ('rhoIk') 
       output_types(n)='scalar'
       out_buf=0.0_WP
       ! Output inverse density at zeta = 0
       out_buf(:,:,1)=real(poly_eval2(rhoIk,0.0_WP),SP)

    case ('rhoIk2') 
       output_types(n)='scalar'
       out_buf=0.0_WP
       ! Output 2nd inverse density weight
       if (ndof.gt.1) then
          out_buf(:,:,1)=real(rhoIk(:,:,2),SP)
       end if

    case ('rho_Var')
       output_types(n)='scalar'
       ! Second moment
       out_buf=0.0_WP
       do nn=2,ndof
          out_buf(:,:,1)=out_buf(:,:,1)+real((rhok(:,:,nn)**2)*Var(nn),SP)
       end do

    ! Check numbers
    ! case ('checkGrad')
    !    output_types(n)='vector'
    !    output_compNames(:,n)=(/ 'Gdx', 'Gdy' /)
    !    out_buf=0.0_WP
    !    ! do i = imino_,imaxo_
    !    !    do j = jmino_,jmaxo_
    !    !       out_buf(i,j,1) = -xm(i)/sqrt(xm(i)**2 + ym(j)**2)
    !    !       out_buf(i,j,2) = -ym(j)/sqrt(xm(i)**2 + ym(j)**2)
    !    !    end do
    !    ! end do
    !    ! out_buf(:,:,1) = Gdx(:,:,1)/sqrt(Gdx(:,:,1)**2 + Gdy(:,:,1)**2)
    !    ! out_buf(:,:,2) = Gdy(:,:,1)/sqrt(Gdx(:,:,1)**2 + Gdy(:,:,1)**2)
    !    out_buf(:,:,1) = Gdx(:,:,1)
    !    out_buf(:,:,2) = Gdy(:,:,1)

    ! case ('checkDiv')
    !    output_types(n)='scalar'
    !    out_buf=0.0_WP
    !    do i = imino_,imaxo_
    !       do j = jmino_,jmaxo_
    !          out_buf(i,j,1) = (((-xm(i)**2 - ym(j)**2)/((xm(i)**2 + ym(j)**2)**1.5_WP)) + 2.0_WP/sqrt(xm(i)**2 + ym(j)**2))
    !       end do
    !    end do

    ! case ('checkTension')
    !    output_types(n)='vector'
    !    output_compNames(:,n)=(/ 'Tx', 'Ty' /)
    !    out_buf=0.0_WP
    !    do i = imino_,imaxo_
    !       do j = jmino_,jmaxo_
    !          out_buf(i,j,1) = -Kx(i,j,1)*(((-xm(i)**2 - ym(j)**2)/((xm(i)**2 + &
    !               ym(j)**2)**1.5_WP)) + 2.0_WP/sqrt(xm(i)**2 + ym(j)**2))/Ks(i,j,1)
    !          out_buf(i,j,2) = -Ky(i,j,1)*(((-xm(i)**2 - ym(j)**2)/((xm(i)**2 + &
    !               ym(j)**2)**1.5_WP)) + 2.0_WP/sqrt(xm(i)**2 + ym(j)**2))/Ks(i,j,1)
    !       end do
    !    end do
       
    ! Unknown output
    case default
       call die('Unknown Visit output:'//trim(output_names(n))//'!')
    end select
    
    return
  end subroutine visit_getData
  
end module visit

! ======================================================================================================== !
! ======================================================================================================== !
!        You do not have to modify the code below this point to add a variable on a pre-defined mesh       !
! ======================================================================================================== !
! ======================================================================================================== !

! ======================================== !
!  Dump Visit Silo files - Initialization  !
! ======================================== !
subroutine visit_init
  use visit
  use io
  implicit none
  integer :: i,iunit,nchar
  logical :: file_exists
  character(len=5)  :: tmpchar
  character(len=60) :: tmpchar2
  character(len=22) :: buffer
  real(WP) :: current_time
  real(WP),dimension(:),allocatable :: visit_times

  ! Read VisIt outputs from inputs
  call count_input('VisIt outputs',output_n)
  allocate(output_names(1:output_n))
  call read_input('VisIt outputs',output_names)
  ! Read output frequency form inputs
  call read_input('VisIt write freq',visit_freq)
  
  ! Check for multiUQ.visit file and create folder if needed
  if (rank.eq.root) then
     inquire(file='Visit/multiUQ.visit',exist=file_exists)
     if (file_exists) then
        ! Get number of lines in file
        iunit = 32
        open(iunit,file="Visit/multiUQ.visit",form="formatted",iostat=ierr,status='old')  
        nout_time=0
        do
           read(iunit,*,end=1)
           nout_time=nout_time+1
        end do
1       continue
        close(iunit)        
        ! Read the file and keep times less than current time
        allocate(visit_times(nout_time))
        iunit = 32
        open(iunit,file="Visit/multiUQ.visit",form="formatted",iostat=ierr,status='old')  
        ! Get current time (formatted correctly)
        write(buffer,'(ES12.5)') time-dt*1e-10_WP
        read(buffer,*) current_time
        do i=1,nout_time
           ! Read file
           read(iunit,'(5A,60A)') tmpchar,tmpchar2
           ! Extract time from string
           nchar=len_trim(tmpchar2)
           read(tmpchar2(1:nchar-11),*) visit_times(i)
           ! Check if it is in the future and exit if true
           if (visit_times(i).ge.current_time) then
              nout_time=i-1
              exit
           end if
        end do
        close(iunit)
        ! Write new file with only past times
        iunit = 32
        open(iunit,file="Visit/multiUQ.visit",form="formatted",iostat=ierr,status='replace')  
        do i=1,nout_time
           write(tmpchar2,'(ES12.5)') visit_times(i)
           tmpchar2='time_'//trim(adjustl(tmpchar2))//'/Visit.silo'
           write(iunit,'(A)') trim(adjustl(tmpchar2))
        end do
        deallocate(visit_times)
        close(iunit)
     else
        nout_time=0
        ! Create visit directory
        if (rank.eq.root) &
             call system('mkdir -p Visit')
     end if
  end if

  ! Set visit counter
  visit_count=1
  
  ! Number of procs per file
  Nproc_node=16
  
  ! Create group_ids 
  allocate(group_ids(nproc))
  do i=1,nproc
     group_ids(i)=ceiling(real(i,WP)/real(Nproc_node,WP))
  end do
  ! Create proc_nums
  allocate(proc_nums(nproc))
  do i=1,nproc
     proc_nums(i)=mod(i-1,Nproc_node)+1
  end do
  
  ! Split procs into new communicators
  call MPI_COMM_SPLIT(COMM,group_ids(rank+1),MPI_UNDEFINED,silo_comm,ierr)
  call MPI_COMM_RANK(silo_comm,silo_rank,ierr)
  silo_rank=silo_rank+1
  
  ! Parse variables to output
  !call parser_getsize('Visit outputs',output_n)
  !allocate(output_names(output_n))
  !call parser_readchararray('Visit outputs',output_names(1:output_n))
  
  ! Allocate arrays
  allocate(out_buf(imino_:imaxo_,jmino_:jmaxo_,2))
  output_max=3*output_n
  allocate(output_types(output_max))
  allocate(output_compNames(3,output_n))
  allocate(  spatial_extents(4,nproc)); allocate( myspatial_extents(4,nproc))
  allocate(  data_extents(2,nproc,2,output_max)) 
  allocate(mydata_extents(2,nproc,2,output_max))
  allocate(names (nproc))
  allocate(lnames(nproc))
  allocate(types (nproc))
  
  ! Create output limits because Visit doesn't support external ghost cells
  noverxmin_out=nGhostVisit; noverxmax_out=nGhostVisit; imin_out=imin_-nGhostVisit; imax_out=imax_+nGhostVisit
  noverymin_out=nGhostVisit; noverymax_out=nGhostVisit; jmin_out=jmin_-nGhostVisit; jmax_out=jmax_+nGhostVisit
  if (rankx.eq.0   ) then; noverxmin_out=0; imin_out=imin_; end if
  if (ranky.eq.0   ) then; noverymin_out=0; jmin_out=jmin_; end if
  if (rankx.eq.px-1) then; noverxmax_out=0; imax_out=imax_; end if
  if (ranky.eq.py-1) then; noverymax_out=0; jmax_out=jmax_; end if
  nxout=imax_out-imin_out+1
  nyout=jmax_out-jmin_out+1
  hi_offset(1)=noverxmax_out
  hi_offset(2)=noverymax_out
  lo_offset(1)=noverxmin_out
  lo_offset(2)=noverymin_out
  
  return 
end subroutine visit_init


! ===================================== !
!  Dump Visit Silo files - Write Files  !
! ===================================== !
subroutine visit_data
  use visit
  implicit none
  integer :: i,n,out
  integer :: dbfile,err,optlist,iunit
  character(len=str_medium) :: med_buffer
  character(len=5) :: dirname
  character(len=38) :: siloname
  character(len=22) :: folder
  character(len=11) :: buffer
  logical :: file_exists
  
  ! Update output counter
  nout_time=nout_time+1

  ! Check if output this step
  visit_count=visit_count-1
  if (visit_count.gt.0) then
     return
  else 
     ! Reset counter
     visit_count=visit_freq
     ! Continue to write VisIt output
  end if

  ! Make foldername
  write(buffer,'(ES11.5)') time
  folder = 'Visit/time_'//buffer

  if (rank.eq.root) then
     ! Create new directory
     ! call system('mkdir -p '// folder)
     i = mkdir(folder, int(o'772',c_int16_t))
  end if
  call MPI_BARRIER(COMM,ierr)

  !  Create the group silo databases 
  ! =============================================

  ! Create the silo name for this processor
  write(siloname,'(A,I5.5,A)') folder//'/group',group_ids(rank+1),'.silo'

  ! Create the silo database
  if (silo_rank.eq.1) then
     err = dbcreate(siloname, len_trim(siloname), DB_CLOBBER, DB_LOCAL,"Silo database created with multiUQ", 34, DB_HDF5, dbfile)
     if(dbfile.eq.-1) call die('Could not create Silo file!')
     ierr = dbclose(dbfile)
  end if
  call MPI_BARRIER(silo_comm,ierr)
  
  !  Write the data
  ! =============================================
  do n=1,Nproc_node
     if (n.eq.silo_rank) then

        ! Open silo file 
        err = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)

        ! Create the silo directory
        write(dirname,'(I5.5)') proc_nums(rank+1)
        err = dbmkdir(dbfile,dirname,5,ierr)
        err = dbsetdir(dbfile,dirname,5)

        ! Write the variables
        mydata_extents=0.0_WP
        do out=1,output_n

           ! Get data to write
           call visit_getData(out)
           
           select case(trim(output_types(out)))

           case ('scalar') ! ---------------------------------------------------------------------------------------
              ! Write the scalar
              med_buffer=output_names(out)
              err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
                   "Mesh", 4, out_buf(imin_out:imax_out,jmin_out:jmax_out,1), &
                   (/nxout,nyout/), 2, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
              mydata_extents(1,rank+1,1,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out,1))
              mydata_extents(2,rank+1,1,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out,1))

           case ('vector') ! ---------------------------------------------------------------------------------------
              ! Write the components
              do i=1,2
                 med_buffer=output_compNames(i,out)
                 err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
                      "Mesh", 4, out_buf(imin_out:imax_out,jmin_out:jmax_out,i), &
                      (/nxout,nyout/), 2, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
                 mydata_extents(1,rank+1,i,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out,i))
                 mydata_extents(2,rank+1,i,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out,i))
              end do
              ! Write the expression
              med_buffer='{'//output_compNames(1,out)//','//output_compNames(2,out)//'}'
              err = dbputdefvars(dbfile,'defvars',7,1,output_names(out),len_trim(output_names(out)), &
                   DB_VARTYPE_VECTOR,med_buffer,len_trim(med_buffer),DB_F77NULL,ierr)
                                           
           case default ! ---------------------------------------------------------------------------------------------
              call die('output_type not defined correctly for '//trim(output_names(out))//' in data write')
           end select
              
        end do

        !  Write the meshes
        ! =============================================
        ! Write the regular mesh
        err = dbmkoptlist(2, optlist)
        err = dbaddiopt(optlist, DBOPT_HI_OFFSET, hi_offset)
        err = dbaddiopt(optlist, DBOPT_LO_OFFSET, lo_offset)
        err = dbputqm(dbfile,"Mesh",4,'xc',2,'yc',2,"zc",2, &
             real(x(imin_out:imax_out+1),WP),real(y(jmin_out:jmax_out+1),WP),DB_F77NULL, &
             (/nxout+1,nyout+1/),2,DB_DOUBLE,DB_COLLINEAR,optlist,ierr)
        err = dbfreeoptlist(optlist)

        ! Spatial extents (written in master silo's multimesh)
        myspatial_extents=0.0_WP
        myspatial_extents(1:2,rank+1)=(/x(imin_  ),y(jmin_  )/)
        myspatial_extents(3:4,rank+1)=(/x(imax_+1),y(jmax_+1)/)

        ! Close silo file
        ierr = dbclose(dbfile)
     end if
     call MPI_BARRIER(silo_comm,ierr)
  end do

  ! Communicate extents
  call MPI_ALLREDUCE( myspatial_extents, spatial_extents,4*nproc,mpi_double_precision,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(mydata_extents,data_extents,2*nproc*2*output_max,mpi_double_precision,MPI_SUM,COMM,ierr)

  ! Create the master silo database (.visit file, multimesh, multivars)
  ! ===============================================================================
  if (rank.eq.root) then
     ! Append .visit file
     iunit = 32
     inquire(file="Visit/multiUQ.visit",exist=file_exists)
     if (file_exists) then
        open(iunit,file="Visit/multiUQ.visit",form="formatted",status="old",position="append",action="write")
     else
        open(iunit,file="Visit/multiUQ.visit",form="formatted",status="new",action="write")
     end if
     write(iunit,'(A)') folder(7:)//'/Visit.silo'
     close(iunit)
     
     ! Set length of names
     err = dbset2dstrlen(len(names))
     
     ! Create the master silo database
     err = dbcreate(folder//'/Visit.silo', len_trim(folder//'/Visit.silo'), DB_CLOBBER, DB_LOCAL, &
          "Silo database created with multiUQ", 34, DB_HDF5, dbfile)
     if(dbfile.eq.-1) call die('Could not create Silo file!')

     !  Write the multimeshes
     ! =======================
     do i=1,nproc
        write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/Mesh'
        lnames(i)=len_trim(names(i))
        types(i)=DB_QUAD_RECT
     end do
     err = dbmkoptlist(4,optlist)
     err = dbaddiopt(optlist, DBOPT_DTIME, time);
     err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
     err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 4)
     err = dbadddopt(optlist, DBOPT_EXTENTS, spatial_extents)
     err = dbputmmesh(dbfile,  "Mesh", 4, nproc, names,lnames, types, optlist, ierr)
     err = dbfreeoptlist(optlist)

     !  Write the multivars
     ! =======================
     do out=1,output_n
        select case(trim(output_types(out)))

        case ('scalar','scalar_Ucell','scalar_Vcell','scalar_Wcell')
           do i=1,nproc
              write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/'//trim(output_names(out))
              lnames(i)=len_trim(names(i))
              types(i)=DB_QUADVAR
           end do
           err = dbmkoptlist(2, optlist)
           err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 2)
           err = dbadddopt(optlist, DBOPT_EXTENTS, data_extents(:,:,1,out))
           err = dbputmvar(dbfile,output_names(out),len_trim(output_names(out)),nproc,names,lnames,types,optlist,ierr)
           err = dbfreeoptlist(optlist)

        case ('vector','vector_Ucell','vector_Vcell','vector_Wcell')
           do n=1,2
              do i=1,nproc
                 write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/'//trim(output_compNames(n,out))
                 lnames(i)=len_trim(names(i))
                 types(i)=DB_QUADVAR
              end do
              err = dbmkoptlist(2, optlist)
              err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 2)
              err = dbadddopt(optlist, DBOPT_EXTENTS, data_extents(:,:,n,out))
              err = dbputmvar(dbfile,output_compNames(n,out),len_trim(output_compNames(n,out)), &
                   nproc,names,lnames,types,optlist,ierr)
              err = dbfreeoptlist(optlist)
           end do
           ! Write the expression
           med_buffer='{'//trim(output_compNames(1,out))// &
                ','//trim(output_compNames(2,out))//'}'
           err = dbputdefvars(dbfile,output_names(out),len_trim(output_names(out)),1, &
                output_names(out),len_trim(output_names(out)), &
                DB_VARTYPE_VECTOR,med_buffer,len_trim(med_buffer),DB_F77NULL,ierr)

        case default
           call die('output_type not defined correctly for '//trim(output_names(out))//'in multivar write')
        end select

     end do

     ! Close database
     ierr = dbclose(dbfile)
  end if
    
  return
end subroutine visit_data
  
