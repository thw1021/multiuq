! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module fourier
  use iso_c_binding
  use communication
  use variables
  use parallel
  use grid
  use math
  implicit none
  include 'fftw3-mpi.f03'

  ! FFT directions
  logical :: fft_x,fct_x
  logical :: fft_y,fct_y

  ! FFTW variables
  real(WP), dimension(:), allocatable :: in_x,out_x
  real(WP), dimension(:), allocatable :: in_y,out_y
  real(WP), dimension(:,:), allocatable :: transpose_fftx
  real(WP), dimension(:,:), allocatable :: transpose_ffty
  integer(KIND=8) :: fplan_x,bplan_x
  integer(KIND=8) :: fplan_y,bplan_y

  ! Oddball
  logical :: oddball

  ! 2D FFT
  type(C_PTR) :: fplan_2d, bplan_2d
  real(C_DOUBLE), pointer :: in_fft(:,:), out_fft(:,:)
  integer(C_INTPTR_T) :: local_N0, local_N0_start
  integer(C_INTPTR_T) :: local_N1, local_N1_start
  type(C_PTR) :: fdata, bdata

  ! Transpose partition - X
  integer, dimension(:), allocatable :: imin_x,imax_x
  integer, dimension(:), allocatable :: jmin_x,jmax_x
  integer, dimension(:), allocatable :: nx_x,ny_x
  real(WP), dimension(:,:,:), allocatable :: sendbuf_x,recvbuf_x
  integer :: sendcount_x,recvcount_x
  character(len=str_medium) :: idir_x

  ! Transpose partition - Y
  integer, dimension(:), allocatable :: imin_y,imax_y
  integer, dimension(:), allocatable :: jmin_y,jmax_y
  integer, dimension(:), allocatable :: nx_y,ny_y
  real(WP), dimension(:,:,:), allocatable :: sendbuf_y,recvbuf_y
  integer :: sendcount_y,recvcount_y
  character(len=str_medium) :: idir_y

  ! Only initialize once
  logical :: init_x=.false.
  logical :: init_y=.false.

end module fourier

! !=====================================================!
! !          Initialize fast Fourier transform          !
! !=====================================================!
! subroutine fourier_init
!   use fourier
!   use parallel
!   implicit none

!   ! Check directions
!   fft_x = .false.
!   fft_y = .false.
!   if (xper.eq.1 .and. nx.ne.1) fft_x = .true.
!   if (yper.eq.1 .and. ny.ne.1) fft_y = .true.
!   fct_x = .false.
!   fct_y = .false.
!   ! Might still not be working - need to further debug?
!   !if (xper.eq.0 .and. uniform_x) fct_x = .true.
!   !if (yper.eq.0 .and. uniform_y) fct_y = .true.

!   ! Test if not even number of points
!   if (fft_x .and. mod(nx,2).ne.0) call die('fourier_init: fft only with even number of points')
!   if (fft_y .and. mod(ny,2).ne.0) call die('fourier_init: fft only with even number of points')

!   ! Initialize Fourier directions
!   if (fft_x) then
!      ! Initialize transpose
!      call transpose_init('x')
!      ! Create plan - X
!      allocate(in_x (nx))
!      allocate(out_x(nx))
!      call dfftw_plan_r2r_1d(fplan_x,nx,in_x,out_x,FFTW_R2HC,FFTW_MEASURE)
!      call dfftw_plan_r2r_1d(bplan_x,nx,in_x,out_x,FFTW_HC2R,FFTW_MEASURE)
!      ! Allocate array for transposed data
!      allocate(transpose_fftx(imin:imax,jmin_x(rankx):jmax_x(rankx)))
!   else if (fct_x) then
!      ! Initialize transpose
!      call transpose_init('x')
!      ! Create plan - X
!      allocate(in_x (nx))
!      allocate(out_x(nx))
!      call dfftw_plan_r2r_1d(fplan_x,nx,in_x,out_x,FFTW_REDFT10,FFTW_MEASURE)
!      call dfftw_plan_r2r_1d(bplan_x,nx,in_x,out_x,FFTW_REDFT01,FFTW_MEASURE)
!      ! Allocate array for transposed data
!      allocate(transpose_fftx(imin:imax,jmin_x(rankx):jmax_x(rankx)))
!   end if

!   if (fft_y) then
!      ! Initialize transpose
!      call transpose_init('y')
!      ! Create plan - Y
!      allocate(in_y (ny))
!      allocate(out_y(ny))
!      call dfftw_plan_r2r_1d(fplan_y,ny,in_y,out_y,FFTW_R2HC,FFTW_MEASURE)
!      call dfftw_plan_r2r_1d(bplan_y,ny,in_y,out_y,FFTW_HC2R,FFTW_MEASURE)
!      ! Allocate array for transposed data
!      allocate(transpose_ffty(imin_y(ranky):imax_y(ranky),jmin:jmax))
!   else if (fct_y) then
!      ! Initialize transpose
!      call transpose_init('y')
!      ! Create plan - Y
!      allocate(in_y (ny))
!      allocate(out_y(ny))
!      call dfftw_plan_r2r_1d(fplan_y,ny,in_y,out_y,FFTW_REDFT10,FFTW_MEASURE)
!      call dfftw_plan_r2r_1d(bplan_y,ny,in_y,out_y,FFTW_REDFT01,FFTW_MEASURE)
!      ! Allocate array for transposed data
!      allocate(transpose_ffty(imin_y(ranky):imax_y(ranky),jmin:jmax))
!   end if

!   ! Oddball
!   oddball = .false.
!   if ( (fft_x .or. fct_x .or. nx.eq.1) .and. &
!        (fft_y .or. fct_y .or. ny.eq.1) .and. &
!        rankx.eq.1 .and. ranky.eq.1) oddball = .true.

!   return
! end subroutine fourier_init

!=====================================================!
!          Initialize fast Fourier transform          !
!=====================================================!
subroutine fourier_init
  use fourier
  implicit none

  integer(C_INTPTR_T) :: alloc_localf, alloc_localb
  integer(C_INTPTR_T) :: N0, N1

  ! Initialize parallel environment
  call fftw_mpi_init()

  ! Check directions
  fft_x = .false.
  fft_y = .false.
  if (xper.eq.1 .and. nx.ne.1) fft_x = .true.
  if (yper.eq.1 .and. ny.ne.1) fft_y = .true.

  ! Test if not even number of points
  if (fft_x .and. mod(nx,2).ne.0) call die('fourier_init: fft only with even number of points')
  if (fft_y .and. mod(ny,2).ne.0) call die('fourier_init: fft only with even number of points')

  ! get local data size and allocate (note dimension reversal)
  N0 = nx; N1 = ny
  ! alloc_localf = fftw_mpi_local_size_2d(N1, N0, comm_2D, local_N1, local_N1_start)
  ! alloc_localb = fftw_mpi_local_size_2d(N1, N0, comm_2D, local_N1, local_N1_start)
  alloc_localf = fftw_mpi_local_size_2d_transposed(N0, N1, comm_2D, &
       local_N0, local_N0_start, local_N1, local_N1_start)
  alloc_localb = fftw_mpi_local_size_2d_transposed(N0, N1, comm_2D, &
       local_N0, local_N0_start, local_N1, local_N1_start)
  fdata = fftw_alloc_real(alloc_localf)
  bdata = fftw_alloc_real(alloc_localb)
  ! call c_f_pointer(fdata,  in_fft, [N0,local_N1])
  ! call c_f_pointer(bdata, out_fft, [N0,local_N1])
  call c_f_pointer(fdata,  in_fft, [local_N0,local_N1])
  call c_f_pointer(bdata, out_fft, [local_N0,local_N1])

  ! Create plan using 2-d transform
  fplan_2d = fftw_mpi_plan_r2r_2d(N1, N0, in_fft, out_fft, comm_2D, FFTW_R2HC, FFTW_R2HC, FFTW_PATIENT)
  bplan_2d = fftw_mpi_plan_r2r_2d(N1, N0, in_fft, out_fft, comm_2D, FFTW_HC2R, FFTW_HC2R, FFTW_PATIENT)

  return
end subroutine fourier_init

!=======================================!
!          FFT Pressure Solver          !
!=======================================!
subroutine fourier_pressure(A,B)
  use fourier
  implicit none

  integer, intent(inout) :: A
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_), intent(inout) :: B
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: Ptrans
  integer :: i,j

  ! Zero arrays
  Ptrans = 0.0_WP
  in_fft = 0.0_WP; out_fft = 0.0_WP

  ! Transform right hand side
  in_fft = B(imin_:imax_,jmin_:jmax_)
  ! if (rank.eq.root) then
  !    print*,'in_fft = ',shape(in_fft)
  !    print*,'Prhs   = ',shape(Prhs(imin_:imax_,jmin_:jmax_))
  ! end if
  call fftw_mpi_execute_r2r(fplan_2d, in_fft, out_fft)
  Ptrans(imin_:imax_,jmin_:jmax_) = out_fft

  ! Calculate
  do i = imin_,imax_
     do j = jmin_,jmax_
        Ptrans(i,j) = -Ptrans(i,j)/ &
             (4.0_WP*(sin(pi*real(i,WP)/real(nx,WP))**2 + sin(pi*real(j,WP)/real(ny,WP))**2))
     end do
  end do
  call comm_borders_2d(Ptrans)

  ! Back transform for pressure calculation
  in_fft = Ptrans(imin_:imax_,jmin_:jmax_)
  call fftw_mpi_execute_r2r(bplan_2d, in_fft, out_fft)
  P_diff(imin_:imax_,jmin_:jmax_,A) = out_fft

  return
end subroutine fourier_pressure

!==========================================!
!          Close FFTW and finalize         !
!==========================================!
subroutine fourier_final
  use fourier
  implicit none

  call fftw_destroy_plan(fplan_2d)
  call fftw_destroy_plan(bplan_2d)
  call fftw_free(fdata)
  call fftw_free(bdata)

  return
end subroutine fourier_final


! =========================== !
! Transpose A and perform FFT !
! =========================== !
subroutine fourier_transform(A)
  use fourier
  implicit none

  real(WP), dimension(imin_:imax_,jmin_:jmax_), intent(inout) :: A
  integer :: i,j

  ! Perform transform in each Fourier direction
  if (fft_x.or.fct_x) then
     ! Transpose in X
     call transpose_x(A,transpose_fftx)

     ! Forward transform - X
     do j=jmin_x(rankx),jmax_x(rankx)
        in_x = transpose_fftx(:,j)
        call dfftw_execute(fplan_x)
        transpose_fftx(:,j) = out_x
     end do

     ! Transpose back
     call btranspose_x(transpose_fftx,A)
  end if

  if (fft_y.or.fct_y) then
     ! Transpose in y
     call transpose_y(A,transpose_ffty)

     ! Forward transform - Y
     do i=imin_y(ranky),imax_y(ranky)
        in_y = transpose_ffty(i,:)
        call dfftw_execute(fplan_y)
        transpose_ffty(i,:) = out_y
     end do

     ! Transpose back
     call btranspose_y(transpose_ffty,A)
  end if

  ! Oddball
  if (oddball) A(imin_,jmin_) = 0.0_WP

  return
end subroutine fourier_transform


! ============================== !
! FFT -> real and transpose back !
! ============================== !
subroutine fourier_inverse(A)
  use fourier
  implicit none

  real(WP), dimension(imin_:imax_,jmin_:jmax_), intent(inout) :: A
  integer :: i,j

  ! Transpose RHS and perform transform
  if (fft_x) then
     ! Transpose in X
     call transpose_x(A,transpose_fftx)
     ! Inverse transform
     do j=jmin_x(rankx),jmax_x(rankx)
        in_x = transpose_fftx(:,j)
        call dfftw_execute(bplan_x)
        transpose_fftx(:,j) = out_x/real(nx,WP)
     end do
     ! Transpose back
     call btranspose_x(transpose_fftx,A)
  else if (fct_x) then
     ! Transpose in X
     call transpose_x(A,transpose_fftx)
     ! Inverse transform
     do j=jmin_x(rankx),jmax_x(rankx)
        in_x = transpose_fftx(:,j)
        call dfftw_execute(bplan_x)
        transpose_fftx(:,j) = out_x/real(2.0_WP*nx,WP)
     end do
     ! Transpose back
     call btranspose_x(transpose_fftx,A)
  end if

  if (fft_y) then
     ! Transpose in Y
     call transpose_y(A,transpose_ffty)
     ! Inverse transform - Y
     do i=imin_y(ranky),imax_y(ranky)
        in_y = transpose_ffty(i,:)
        call dfftw_execute(bplan_y)
        transpose_ffty(i,:) = out_y/real(ny,WP)
     end do
     ! Transpose back
     call btranspose_y(transpose_ffty,A)
  else if (fct_y) then
     ! Transpose in Y
     call transpose_y(A,transpose_ffty)
     ! Inverse transform - Y
     do i=imin_y(ranky),imax_y(ranky)
        in_y = transpose_ffty(i,:)
        call dfftw_execute(bplan_y)
        transpose_ffty(i,:) = out_y/real(2.0_WP*ny,WP)
     end do
     ! Transpose back
     call btranspose_y(transpose_ffty,A)
  end if

  return
end subroutine fourier_inverse



!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!


! =============================== !
! Initialize the transpose module !
! =============================== !
subroutine transpose_init(dir)
  use fourier
  implicit none

  integer :: ip,jp
  integer :: qf,rf
  character(len=*) :: dir

  ! Check if already initialized
  if (trim(adjustl(dir)).eq.'x' .and. init_x) return
  if (trim(adjustl(dir)).eq.'y' .and. init_y) return
  if (trim(adjustl(dir)).eq.'x') init_x=.true.
  if (trim(adjustl(dir)).eq.'y') init_y=.true.

  ! Initialize direction for transpose
  select case (trim(adjustl(dir)))
  case ('x')
     ! Determine initial non-decomposed direction used in transpose
     if (px.eq.1) then
        idir_x="xdir"
     else if (py.eq.1 .and. ny.gt.1) then
        idir_x="ydir"
     else if (py.eq.1) then
        idir_x="ydir"
     else
        call die('transpose_init: at least one direction non-decomposed required')
     end if

     ! Allocate global partitions
     allocate(nx_x(px))
     allocate(ny_x(px))
     allocate(imin_x(px))
     allocate(imax_x(px))
     allocate(jmin_x(px))
     allocate(jmax_x(px))

     ! Partition
     select case (trim(idir_x))
     case ('xdir')

        ! No transpose required

        ! Local partition
        nx_x = nx_
        ny_x = ny_
        imin_x = imin_
        imax_x = imax_
        jmin_x = jmin_
        jmax_x = jmax_

     case ('ydir')

        ! Store old local indices from each processor
        call MPI_AllGather(imin_,1,MPI_INTEGER,imin_x,1,MPI_INTEGER,comm_x,ierr)
        call MPI_AllGather(imax_,1,MPI_INTEGER,imax_x,1,MPI_INTEGER,comm_x,ierr)
        nx_x=imax_x-imin_x+1

        ! Partition new local indices
        do ip=1,px
           qf = ny/px
           rf = mod(ny,px)
           if (ip<=rf) then
              ny_x(ip)   = qf+1
              jmin_x(ip) = jmin + (ip-1)*(qf+1)
           else
              ny_x(ip)   = qf
              jmin_x(ip) = jmin + rf*(qf+1) + (ip-rf-1)*qf
           end if
           jmax_x(ip) = jmin_x(ip) + ny_x(ip) - 1
        end do

        ! Variables for AllToAll communication
        sendcount_x = maxval(nx_x)*maxval(ny_x)
        recvcount_x = maxval(nx_x)*maxval(ny_x)
        allocate(sendbuf_x(maxval(nx_x),maxval(ny_x),px))
        allocate(recvbuf_x(maxval(nx_x),maxval(ny_x),px))

        ! Pad buffers with zeros
        sendbuf_x=0.0_WP
        recvbuf_x=0.0_WP

     end select

  case ('y')
     ! Determine initial non-decomposed direction used in transpose
     if (py.eq.1) then
        idir_y="ydir"
     else if (px.eq.1 .and. nx.gt.1) then
        idir_y="xdir"
     else if (px.eq.1) then
        idir_y="xdir"
     else
        call die('transpose_init: at least one direction non-decomposed required')
     end if

     ! Allocate global partitions
     allocate(nx_y(py))
     allocate(ny_y(py))
     allocate(imin_y(py))
     allocate(imax_y(py))
     allocate(jmin_y(py))
     allocate(jmax_y(py))

     ! Partition
     select case (trim(idir_y))
     case ('xdir')

        ! Store old local indices from each processor
        call MPI_AllGather(jmin_,1,MPI_INTEGER,jmin_y,1,MPI_INTEGER,comm_y,ierr)
        call MPI_AllGather(jmax_,1,MPI_INTEGER,jmax_y,1,MPI_INTEGER,comm_y,ierr)
        ny_y=jmax_y-jmin_y+1

        ! Partition new local indices
        do jp=1,py
           qf = nx/py
           rf = mod(nx,py)
           if (jp<=rf) then
              nx_y(jp)   = qf+1
              imin_y(jp) = imin + (jp-1)*(qf+1)
           else
              nx_y(jp)   = qf
              imin_y(jp) = imin + rf*(qf+1) + (jp-rf-1)*qf
           end if
           imax_y(jp) = imin_y(jp) + nx_y(jp) - 1
        end do

        ! Variables for AllToAll communication
        sendcount_y = maxval(nx_y)*maxval(ny_y)
        recvcount_y = maxval(nx_y)*maxval(ny_y)
        allocate(sendbuf_y(maxval(nx_y),maxval(ny_y),py))
        allocate(recvbuf_y(maxval(nx_y),maxval(ny_y),py))

        ! Pad buffers with zeros
        sendbuf_y=0.0_WP
        recvbuf_y=0.0_WP

     case ('ydir')

        ! No transpose required

        ! Local partition
        nx_y = nx_
        ny_y = ny_
        imin_y = imin_
        imax_y = imax_
        jmin_y = jmin_
        jmax_y = jmax_

     end select

  case default
     call die('transpose_init: unknown direction')
  end select

  return
end subroutine transpose_init


! ========================== !
! Transpose A ==> At so      !
! contiguous in X direction  !
! ========================== !
subroutine transpose_x(A,At)
  use fourier
  implicit none

  real(WP), dimension(imin_:imax_,jmin_:jmax_), intent(in) :: A
  real(WP), dimension(imin:imax,jmin_x(rankx):jmax_x(rankx)), intent(out) :: At
  integer :: i,j,ip,ii,jj

  select case (trim(idir_x))
  case ('xdir')

     ! No transpose required
     At = A

  case ('ydir')

     ! Transpose x==>y
     do ip=1,px
        do j=jmin_x(ip),jmax_x(ip)
           do i=imin_,imax_
              jj=j-jmin_x(ip)+1
              ii=i-imin_+1
              sendbuf_x(ii,jj,ip) = A(i,j)
           end do
        end do
     end do

     call MPI_AllToAll(sendbuf_x,sendcount_x,mpi_double_precision,&
          recvbuf_x,recvcount_x,mpi_double_precision,comm_x,ierr)

     do ip=1,px
        do j=jmin_x(rankx),jmax_x(rankx)
           do i=imin_x(ip),imax_x(ip)
              jj=j-jmin_x(rankx)+1
              ii=i-imin_x(ip)+1
              At(i,j)=recvbuf_x(ii,jj,ip)
           end do
        end do
     end do

  end select

  return
end subroutine transpose_x


! ========================== !
! Transpose A ==> At so      !
! contiguous in Y direction  !
! ========================== !
subroutine transpose_y(A,At)
  use fourier
  implicit none

  real(WP), dimension(imin_:imax_,jmin_:jmax_), intent(in) :: A
  real(WP), dimension(imin_y(ranky):imax_y(ranky),jmin:jmax), intent(out) :: At
  integer :: i,j,jp,ii,jj

  select case (trim(idir_y))
  case ('xdir')

     ! Transpose y==>x
     do jp=1,py
        do j=jmin_,jmax_
           do i=imin_y(jp),imax_y(jp)
              ii=i-imin_y(jp)+1
              jj=j-jmin_+1
              sendbuf_y(ii,jj,jp) = A(i,j)
           end do
        end do
     end do

     call MPI_AllToAll(sendbuf_y,sendcount_y,mpi_double_precision,&
          recvbuf_y,recvcount_y,mpi_double_precision,comm_y,ierr)

     do jp=1,py
        do j=jmin_y(jp),jmax_y(jp)
           do i=imin_y(ranky),imax_y(ranky)
              ii=i-imin_y(ranky)+1
              jj=j-jmin_y(jp)+1
              At(i,j)=recvbuf_y(ii,jj,jp)
           end do
        end do
     end do

  case ('ydir')

     ! No transpose required
     At = A

  end select

  return
end subroutine transpose_y


! ========================!
! Back transpose At ==> A !
! from X direction        !
! ======================= !
subroutine btranspose_x(At,A)
  use fourier
  implicit none

  real(WP), dimension(imin:imax,jmin_x(rankx):jmax_x(rankx)), intent(in) :: At
  real(WP), dimension(imin_:imax_,jmin_:jmax_), intent(out) :: A
  integer :: i,j,ip,ii,jj

  select case (trim(idir_x))
  case ('xdir')

     ! No transpose required
     A = At

  case ('ydir')

     ! Transpose y==>x
     do ip=1,px
        do j=jmin_x(rankx),jmax_x(rankx)
           do i=imin_x(ip),imax_x(ip)
              jj=j-jmin_x(rankx)+1
              ii=i-imin_x(ip)+1
              sendbuf_x(ii,jj,ip) = At(i,j)
           end do
        end do
     end do

     call MPI_AllToAll(sendbuf_x,sendcount_x,mpi_double_precision,&
          recvbuf_x,recvcount_x,mpi_double_precision,comm_x,ierr)

     do ip=1,px
        do j=jmin_x(ip),jmax_x(ip)
           do i=imin_x(rankx),imax_x(rankx)
              jj=j-jmin_x(ip)+1
              ii=i-imin_x(rankx)+1
              A(i,j)=recvbuf_x(ii,jj,ip)
           end do
        end do
     end do

  end select

  return
end subroutine btranspose_x


! ========================!
! Back transpose At ==> A !
! from Y direction        !
! ======================= !
subroutine btranspose_y(At,A)
  use fourier
  implicit none

  real(WP), dimension(imin_y(ranky):imax_y(ranky),jmin:jmax), intent(in) :: At
  real(WP), dimension(imin_:imax_,jmin_:jmax_), intent(out) :: A
  integer :: i,j,jp,ii,jj

  select case (trim(idir_y))
  case ('xdir')

     ! Transpose x==>y
     do jp=1,py
        do j=jmin_y(jp),jmax_y(jp)
           do i=imin_y(ranky),imax_y(ranky)
              ii=i-imin_y(ranky)+1
              jj=j-jmin_y(jp)+1
              sendbuf_y(ii,jj,jp) = At(i,j)
           end do
        end do
     end do

     call MPI_AllToAll(sendbuf_y,sendcount_y,mpi_double_precision,&
          recvbuf_y,recvcount_y,mpi_double_precision,comm_y,ierr)

     do jp=1,py
        do j=jmin_y(ranky),jmax_y(ranky)
           do i=imin_y(jp),imax_y(jp)
              ii=i-imin_y(jp)+1
              jj=j-jmin_y(ranky)+1
              A(i,j)=recvbuf_y(ii,jj,jp)
           end do
        end do
     end do

  case ('ydir')

     ! No transpose required
     A = At

  end select

  return
end subroutine btranspose_y

