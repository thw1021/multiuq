! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

! ========================================================= !
! Calculation of Points and Weights for Gaussian Quadrature !
! ========================================================= !
module quadrature
  use variables
  use basis
  use polynomial
  implicit none


end module quadrature

!==============================!
!        Initialization        !
!==============================!
subroutine quadrature_init
  use quadrature
  use io
  implicit none

  integer :: k,q
  real(WP) :: psum

  ! Read Number of Points from input file
  call read_input('Quad Points',quadpts)

  ! Allocate Weights and Points
  allocate(Wgauss(1:quadpts,1))
  allocate(Rgauss(1:quadpts,1))

  ! ! Calculate the Points
  ! do k=1,quadpts
  !    Rgauss(k,1) = -1 + (2/(quadpts-1))*(k-1)
  ! end do

  ! ! Calculate the Weights
  ! do k=1,quadpts
  !    psum = 0.0_WP
  !    do q=1,phi(k)%n
  !       psum = psum + phi(k)%a(q)*phi(k)%p(q)*Rgauss(k,1)**(phi(k)%p(q)-1)
  !    end do
  !    Wgauss(k,1) = 2/((1-Rgauss(k,1)**2)*psum**2)
  ! end do

  ! ! Temporary Points and Weights
  ! Rgauss(1,1) = 0.339981043583856
  ! Rgauss(2,1) = -0.339981043583856
  ! Rgauss(3,1) = 0.861136311590453
  ! Rgauss(4,1) = -0.861136311590453

  ! Wgauss(1,1) = 0.652145154862526
  ! Wgauss(2,1) = 0.652145154862526
  ! Wgauss(3,1) = 0.347854845137454
  ! Wgauss(4,1) = 0.347854845137454

  ! Temporary 20 Point Gaussian
  Wgauss(1,1) = 0.1527533871307258
  Wgauss(2,1) = 0.1527533871307258
  Wgauss(3,1) = 0.1491729864726037
  Wgauss(4,1) = 0.1491729864726037
  Wgauss(5,1) = 0.1420961093183820
  Wgauss(6,1) = 0.1420961093183820
  Wgauss(7,1) = 0.1316886384491766
  Wgauss(8,1) = 0.1316886384491766
  Wgauss(9,1) = 0.1181945319615184
  Wgauss(10,1) = 0.1181945319615184
  Wgauss(11,1) = 0.1019301198172404
  Wgauss(12,1) = 0.1019301198172404
  Wgauss(13,1) = 0.0832767415767048
  Wgauss(14,1) = 0.0832767415767048
  Wgauss(15,1) = 0.0626720483341091
  Wgauss(16,1) = 0.0626720483341091
  Wgauss(17,1) = 0.0406014298003869
  Wgauss(18,1) = 0.0406014298003869
  Wgauss(19,1) = 0.0176140071391521
  Wgauss(20,1) = 0.0176140071391521

  Rgauss(1,1) = -0.0765265211334973
  Rgauss(2,1) = 0.0765265211334973
  Rgauss(3,1) = -0.2277858511416451
  Rgauss(4,1) = 0.2277858511416451
  Rgauss(5,1) = -0.3737060887154195
  Rgauss(6,1) = 0.3737060887154195
  Rgauss(7,1) = -0.5108670019508271
  Rgauss(8,1) = 0.5108670019508271
  Rgauss(9,1) = -0.6360536807265150
  Rgauss(10,1) = 0.6360536807265150
  Rgauss(11,1) = -0.7463319064601508
  Rgauss(12,1) = 0.7463319064601508
  Rgauss(13,1) = -0.8391169718222188
  Rgauss(14,1) = 0.8391169718222188
  Rgauss(15,1) = -0.9122344282513259
  Rgauss(16,1) = 0.9122344282513259
  Rgauss(17,1) = -0.9639719272779138
  Rgauss(18,1) = 0.9639719272779138
  Rgauss(19,1) = -0.9931285991850949
  Rgauss(20,1) = 0.9931285991850949

end subroutine quadrature_init
