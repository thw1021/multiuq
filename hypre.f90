! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module hypre
  use variables
  use pressure
  use parallel
  implicit none

  ! Dimension of the problem
  integer :: dim = 2

  ! HYPRE objects
  integer(kind=8) :: Hgrid
  integer(kind=8) :: stencil
  integer(kind=8) :: matrix
  integer(kind=8) :: rhs
  integer(kind=8) :: sol
  integer(kind=8) :: solver
  integer(kind=8) :: precond

  ! Preconditioner info
  integer :: precond_id

  ! PFMG relaxation info
  character(len=str_medium) :: relax
  integer :: relax_type

  ! Operator info
  integer,  dimension(:), allocatable :: sten_ind
  real(WP), dimension(:), allocatable :: val
  real(WP), dimension(:), allocatable :: val_L, val_R, val_T, val_B
  real(WP), dimension(:), allocatable :: val_LB, val_RB, val_LT, val_RT
  integer :: stencil_size, stcp

end module hypre


!================================================!
!          Initialize the HYPRE package          !
!================================================!
subroutine hypre_init
  use hypre
  use io
  implicit none

  integer :: i,j,k,count
  integer :: log_level,dscg_max_iter,pcg_max_iter,solver_type
  integer, dimension(2) :: lower,upper
  integer, dimension(2) :: offset
  integer, dimension(4) :: num_ghost
  integer, dimension(2) :: periodicity_hypre
  integer, dimension(2) :: ind
  real(WP) :: conv_tol

  ! Which pressure solver?
  call read_input('Which pressure solver?',pressure_solver)
  call read_input('Which pre-conditioner?',pressure_precond)

  !===== Set up STRUCT grid =====!

  ! Create grid object
  call HYPRE_StructGridCreate(COMM,dim,Hgrid,ierr)

  ! Set grid extents
  lower(1) = imin_; lower(2) = jmin_
  upper(1) = imax_; upper(2) = jmax_
  call HYPRE_StructGridSetExtents(Hgrid,lower(1:dim),upper(1:dim),ierr)

  ! Determine periodicity
  periodicity_hypre = 0
  if (isxper) periodicity_hypre(1) = nx
  if (isyper) periodicity_hypre(2) = ny
  call HYPRE_StructGridSetPeriodic(Hgrid,periodicity_hypre(1:dim),ierr)

  ! Set ghost cells (for 5 point stencil)
  num_ghost = 1
  call HYPRE_StructGridSetNumGhost(Hgrid,num_ghost(1:2*dim),ierr)

  ! Assemble the grid
  call HYPRE_StructGridAssemble(Hgrid,ierr)

  !===== Create stencil object =====!

  ! Set up the discretization stencil (start with 5 point stencil)
  stcp = 1
  stencil_size = 2*dim*stcp+1

  ! Create stencil in each direction
  call HYPRE_StructStencilCreate(dim,stencil_size,stencil,ierr)
  count = 0; offset = 0
  call HYPRE_StructStencilSetElement(stencil,count,offset(1:dim),ierr)
  count = count+1
  do k = 1,dim
     do i = -stcp,stcp
        if (i.eq.0) cycle
        offset = 0
        offset(k) = i
        call HYPRE_StructStencilSetElement(stencil,count,offset(1:dim),ierr)
        count = count+1
     end do
  end do

  !===== Set up the operating matrix =====!

  ! Initialize the matrix (currently using a constant operator)
  call HYPRE_StructMatrixCreate(COMM,Hgrid,stencil,matrix,ierr)
  call HYPRE_StructMatrixSetNumGhost(matrix,num_ghost(1:2*dim),ierr)
  call HYPRE_StructMatrixInitialize(matrix,ierr)
  allocate(sten_ind  (0:stencil_size-1))
  allocate(val       (0:stencil_size-1))
  allocate(val_L(0:stencil_size-1)); allocate(val_R(0:stencil_size-1))
  allocate(val_T(0:stencil_size-1)); allocate(val_B(0:stencil_size-1))
  allocate(val_LB(0:stencil_size-1)); allocate(val_LT(0:stencil_size-1))
  allocate(val_RB(0:stencil_size-1)); allocate(val_RT(0:stencil_size-1))
  do i = 0,stencil_size-1
     sten_ind(i) = i
  end do

  ! Matrix coefficients for a 5 point stencil (2nd order central difference)
  val(0) = -2.0_WP*(dxi**2 + dyi**2)
  val(1) = dxi**2; val(2) = dxi**2
  val(3) = dyi**2; val(4) = dyi**2

  ! Set stencil for boundary walls with Neumann condition
  val_L = val; val_R = val; val_T = val; val_B = val
  val_L(0) = val_L(0) + dxi**2; val_L(1) = 0.0_WP
  val_R(0) = val_R(0) + dxi**2; val_R(2) = 0.0_WP
  val_B(0) = val_B(0) + dyi**2; val_B(3) = 0.0_WP
  val_T(0) = val_T(0) + dyi**2; val_T(4) = 0.0_WP

  ! Create stencil for corners with Neuman condition
  val_LB = val; val_LT = val; val_RB = val; val_RT = val
  val_LB(0) = val_LB(0) + dxi**2 + dyi**2; val_LB(1) = 0.0_WP; val_LB(3) = 0.0_WP
  val_LT(0) = val_LT(0) + dxi**2 + dyi**2; val_LT(1) = 0.0_WP; val_LT(4) = 0.0_WP
  val_RB(0) = val_RB(0) + dxi**2 + dyi**2; val_RB(2) = 0.0_WP; val_RB(3) = 0.0_WP
  val_RT(0) = val_RT(0) + dxi**2 + dyi**2; val_RT(2) = 0.0_WP; val_RT(4) = 0.0_WP

  ! Establish matrix coefficients at each mesh point (using stencil)
  do i = imin_,imax_
     do j = jmin_,jmax_
        ! Mesh position
        ind(1) = i; ind(2) = j
        ! Set the values
        call HYPRE_StructMatrixSetValues(matrix,ind(1:dim),stencil_size,sten_ind,val,ierr)
        ! Reset the walls with Neumann condition
        if ((i.eq.imin_).and.(rankx.eq.0)) &
             call HYPRE_StructMatrixSetValues(matrix,ind(1:dim),stencil_size,sten_ind,val_L,ierr)
        if ((i.eq.imax_).and.(rankx.eq.px-1)) &
             call HYPRE_StructMatrixSetValues(matrix,ind(1:dim),stencil_size,sten_ind,val_R,ierr)
        if ((j.eq.jmin_).and.(ranky.eq.0)) &
             call HYPRE_StructMatrixSetValues(matrix,ind(1:dim),stencil_size,sten_ind,val_B,ierr)
        if ((j.eq.jmax_).and.(ranky.eq.py-1)) &
             call HYPRE_StructMatrixSetValues(matrix,ind(1:dim),stencil_size,sten_ind,val_T,ierr)
        ! Reset corners with Neuman condition
        if ((i.eq.imin_).and.(j.eq.jmin_).and.(rankx.eq.0).and.(ranky.eq.0)) &
             call HYPRE_StructMatrixSetValues(matrix,ind(1:dim),stencil_size,sten_ind,val_LB,ierr)
        if ((i.eq.imin_).and.(j.eq.jmax_).and.(rankx.eq.0).and.(ranky.eq.py-1)) &
             call HYPRE_StructMatrixSetValues(matrix,ind(1:dim),stencil_size,sten_ind,val_LT,ierr)
        if ((i.eq.imax_).and.(j.eq.jmin_).and.(rankx.eq.px-1).and.(ranky.eq.0)) &
             call HYPRE_StructMatrixSetValues(matrix,ind(1:dim),stencil_size,sten_ind,val_RB,ierr)
        if ((i.eq.imax_).and.(j.eq.jmax_).and.(rankx.eq.px-1).and.(ranky.eq.py-1)) &
             call HYPRE_StructMatrixSetValues(matrix,ind(1:dim),stencil_size,sten_ind,val_RT,ierr)
     end do
  end do

  ! Hold for all processors
  call MPI_BARRIER(COMM, ierr)

  ! Assemble final A matrix (used for entire simulation)
  call HYPRE_StructMatrixAssemble(matrix,ierr)

  ! Deallocate arrays
  deallocate(sten_ind); deallocate(val)
  deallocate(val_L);  deallocate(val_R);  deallocate(val_T);  deallocate(val_B)
  deallocate(val_LB); deallocate(val_LT); deallocate(val_RB); deallocate(val_RT)

  ! Prepare RHS
  call HYPRE_StructVectorCreate(COMM,Hgrid,rhs,ierr)
  call HYPRE_StructVectorInitialize(rhs,ierr)
  call HYPRE_StructVectorAssemble(rhs,ierr)

  ! Create solution vector
  call HYPRE_StructVectorCreate(COMM,Hgrid,sol,ierr)
  call HYPRE_StructVectorInitialize(sol,ierr)
  call HYPRE_StructVectorAssemble(sol,ierr)

  ! Initialize the preconditioner & relaxation scheme
  call hypre_precond_init

  ! Setup the solver
  log_level = 1 ! 1 for some info (res,it)
  select case(trim(pressure_solver))
  case('PFMG')
     call HYPRE_StructPFMGCreate(COMM,solver,ierr)
     call HYPRE_StructPFMGSetMaxIter(solver,pressure_max_steps,ierr)
     call HYPRE_StructPFMGSetTol(solver,cvg_criteria,ierr)
     call HYPRE_StructPFMGSetRelaxType(solver,relax_type,ierr)
     call HYPRE_StructPFMGSetLogging(solver,log_level,ierr)
     call HYPRE_StructPFMGSetup(solver,matrix,rhs,sol,ierr)
  case('SMG')
     call HYPRE_StructSMGCreate(COMM,solver,ierr)
     call HYPRE_StructSMGSetMaxIter(solver,pressure_max_steps,ierr)
     call HYPRE_StructSMGSetTol(solver,cvg_criteria,ierr)
     call HYPRE_StructSMGSetLogging(solver,log_level,ierr)
     call HYPRE_StructSMGSetup(solver,matrix,rhs,sol,ierr)
  case('PCG')
     call HYPRE_StructPCGCreate(COMM,solver,ierr)
     call HYPRE_StructPCGSetMaxIter(solver,pressure_max_steps,ierr)
     call HYPRE_StructPCGSetTol(solver,cvg_criteria,ierr)
     call HYPRE_StructPCGSetLogging(solver,log_level,ierr)
     if (precond_id.ge.0) call HYPRE_StructPCGSetPrecond(solver,precond_id,precond,ierr)
     call HYPRE_StructPCGSetup(solver,matrix,rhs,sol,ierr)
  case('BICGSTAB')
     call HYPRE_StructBICGSTABCreate(COMM,solver,ierr)
     call HYPRE_StructBICGSTABSetMaxIter(solver,pressure_max_steps,ierr)
     call HYPRE_StructBICGSTABSetTol(solver,cvg_criteria,ierr)
     call HYPRE_StructBICGSTABSetLogging(solver,log_level,ierr)
     if (precond_id.ge.0) call HYPRE_StructBICGSTABSetPrecond(solver,precond_id,precond,ierr)
     call HYPRE_StructBICGSTABSetup(solver,matrix,rhs,sol,ierr)
  case('GMRES')
     call HYPRE_StructGMRESCreate(COMM,solver,ierr)
     call HYPRE_StructGMRESSetMaxIter(solver,pressure_max_steps,ierr)
     call HYPRE_StructGMRESSetTol(solver,cvg_criteria,ierr)
     call HYPRE_StructGMRESSetLogging(solver,log_level,ierr)
     if (precond_id.ge.0) call HYPRE_StructGMRESSetPrecond(solver,precond_id,precond,ierr)
     call HYPRE_StructGMRESSetup(solver,matrix,rhs,sol,ierr)
  case ('HYBRID')
     call HYPRE_StructHYBRIDCreate(COMM,solver,ierr)
     dscg_max_iter = 2*pressure_max_steps
     call HYPRE_StructHYBRIDSetDSCGMaxIte(solver,dscg_max_iter,ierr)
     pcg_max_iter = pressure_max_steps
     call HYPRE_StructHYBRIDSetPCGMaxIter(solver,pcg_max_iter,ierr)
     call HYPRE_StructHYBRIDSetTol(solver,cvg_criteria,ierr)
     conv_tol = 0.9_WP
     call HYPRE_StructHYBRIDSetConvergenc(solver,conv_tol,ierr)
     call HYPRE_StructHYBRIDSetLogging(solver,log_level,ierr)
     solver_type = 1
     call HYPRE_StructHybridSetSolverType(solver,solver_type,ierr)
     if (precond_id.ge.0) call HYPRE_StructHYBRIDSetPrecond(solver,precond_id,precond,ierr)
     call HYPRE_StructHYBRIDSetup(solver,matrix,rhs,sol,ierr)
  end select

  return
end subroutine hypre_init

!===================================================!
!          Initialize HYPRE preconditioner          !
!===================================================!
subroutine hypre_precond_init
  use hypre
  implicit none

  integer  :: precond_max_iter
  real(WP) :: precond_cvg

  ! Initialize relaxation scheme for PFMG
  if (trim(pressure_solver).eq.'PFMG') then
     call read_input('PFMG relaxation scheme?',relax)
     select case (trim(relax))
     case ('Jacobi')
        relax_type=0
     case ('weightedJacobi')
        relax_type=1
     case ('symRBGS')
        relax_type=2
     case ('non-symRBGS')
        relax_type=3
     case default
        call die('Unknown relaxation type (hypre.f90): '//trim(relax))
     end select
  end if

  ! Initialize preconditioner
  select case(trim(pressure_precond))
  case('PFMG')
     call HYPRE_StructPFMGCreate(COMM,precond,ierr)
     precond_max_iter=1
     call HYPRE_StructPFMGSetMaxIter(precond,precond_max_iter,ierr)
     precond_cvg=0.0_WP
     call HYPRE_StructPFMGSetTol(precond,precond_cvg,ierr)
     call HYPRE_StructPFMGSetRelaxType(precond,relax_type,ierr)
     precond_id = 1
  case('SMG')
     call HYPRE_StructSMGCreate(COMM,precond,ierr)
     precond_max_iter=1
     call HYPRE_StructSMGSetMaxIter(precond,precond_max_iter,ierr)
     precond_cvg=0.0_WP
     call HYPRE_StructSMGSetTol(precond,precond_cvg,ierr)
     precond_id = 0
  case('DIAGONAL')
     precond = 0
     precond_id = 8
  case ('none')
     precond_id = -1
  case default
     call die('Hypre_precond_init: Unknown HYPRE preconditioner')
  end select

  return
end subroutine hypre_precond_init


!=========================================!
!          Transfer RHS to HYPRE          !
!=========================================!
subroutine hypre_in(dd)
  use hypre
  implicit none

  integer :: i,j
  integer, intent(in) :: dd
  integer, dimension(2) :: ind

  ! Import rhs and old pressure
  do j = jmin_,jmax_
     do i = imin_,imax_
        ! Mesh position
        ind(1) = i; ind(2) = j
        ! Store values
        call HYPRE_StructVectorSetValues(rhs,ind(1:dim),Prhs(i,j),ierr)
        call HYPRE_StructVectorSetValues(sol,ind(1:dim),Pr(i,j,dd),ierr)
     end do
  end do
  call HYPRE_StructVectorAssemble(rhs,ierr)
  call HYPRE_StructVectorAssemble(sol,ierr)

  return
end subroutine hypre_in


!================================================!
!          Solve the problem with HYPRE          !
!================================================!
subroutine hypre_solve
  use hypre
  implicit none

  ! Solve
  select case(trim(pressure_solver))
  case('PFMG')
     call HYPRE_StructPFMGSolve(solver,matrix,rhs,sol,ierr)
     call HYPRE_StructPFMGGetNumIteration(solver,pressure_steps,ierr)
     call HYPRE_StructPFMGGetFinalRelativ(solver,P_res,ierr)
  case('SMG')
     call HYPRE_StructSMGSolve(solver,matrix,rhs,sol,ierr)
     call HYPRE_StructSMGGetNumIterations(solver,pressure_steps,ierr)
     call HYPRE_StructSMGGetFinalRelative(solver,P_res,ierr)
  case ('PCG')
     call HYPRE_StructPCGSolve(solver,matrix,rhs,sol,ierr)
     call HYPRE_StructPCGGetNumIterations(solver,pressure_steps,ierr)
     call HYPRE_StructPCGGetFinalRelative(solver,P_res,ierr)
  case ('BICGSTAB')
     call HYPRE_StructBICGSTABSolve(solver,matrix,rhs,sol,ierr)
     call HYPRE_StructBICGSTABGetNumItera(solver,pressure_steps,ierr)
     call HYPRE_StructBICGSTABGetFinalRel(solver,P_res,ierr)
  case ('GMRES')
     call HYPRE_StructGMRESSolve(solver,matrix,rhs,sol,ierr)
     call HYPRE_StructGMRESGetNumIteratio(solver,pressure_steps,ierr)
     call HYPRE_StructGMRESGetFinalRelati(solver,P_res,ierr)
  case ('HYBRID')
     call HYPRE_StructHYBRIDSolve(solver,matrix,rhs,sol,ierr)
     call HYPRE_StructHYBRIDGetNumIterati(solver,pressure_steps,ierr)
     call HYPRE_StructHYBRIDGetFinalRelat(solver,P_res,ierr)
  end select

  return
end subroutine hypre_solve

!=====================================================!
!          Transfer the solution from HYPRE           !
!=====================================================!
subroutine hypre_out(dd)
  use hypre
  implicit none

  integer :: i,j
  integer, intent(in) :: dd
  integer, dimension(2) :: ind

  do j = jmin_,jmax_
     do i = imin_,imax_
        ! Mesh position
        ind(1) = i; ind(2) = j
        ! Store values
        call HYPRE_StructVectorGetValues(sol,ind(1:dim),Pr(i,j,dd),ierr)
     end do
  end do

  return
end subroutine hypre_out

!========================================!
!          Close HYPRE package           !
!========================================!
subroutine hypre_final
  use hypre
  implicit none

  ! Destroy solver
  select case(trim(pressure_solver))
  case('PFMG')
     call HYPRE_StructPFMGDestroy(solver,ierr)
  case('SMG')
     call HYPRE_StructSMGDestroy(solver,ierr)
  case('PCG')
     call HYPRE_StructPCGDestroy(solver,ierr)
  case('BICGSTAB')
     call HYPRE_StructBICGSTABDestroy(solver,ierr)
  case('GMRES')
     call HYPRE_StructGMRESDestroy(solver,ierr)
  case ('HYBRID')
     call HYPRE_StructHYBRIDDestroy(solver,ierr)
  end select

  ! Destroy stencil
  call HYPRE_StructStencilDestroy(stencil,ierr)

  ! Destroy matrix and vectors
  call HYPRE_StructMatrixDestroy(matrix,ierr)
  call HYPRE_StructVectorDestroy(rhs,ierr)
  call HYPRE_StructVectorDestroy(sol,ierr)

  ! Destroy grid
  call HYPRE_StructGridDestroy(Hgrid,ierr)

  return
end subroutine hypre_final
