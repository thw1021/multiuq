! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

!
!              Stagard Grid Layout
!
! y(j+1) - ==================================
!          |   cell (i,j)   |  cell (i+1,j) |
!          |                |               |
!          |                |               |
!  ym(j) - |-->    o P      |-->    o P     |
!          |  u             |  u            |
!          |       ^ v      |       ^ v     |
!          |       |        |       |       |
!   y(j) - ==================================
!          |       |        |       |       |
!         x(i)   xm(i)   x(i+1)  xm(i+1)  x(i+2)
!
! Assumptions:
!   - 2D
!   - uniform 

module grid
  use variables
  use parallel
  implicit none

  ! Global variables
  integer :: imin , imax , jmin , jmax
  ! Local variables
  integer :: imin_, imax_, jmin_, jmax_
  integer :: nx_, ny_
  ! Local variables with ghost cells
  integer :: imino_, imaxo_, jmino_, jmaxo_
  integer :: nxo_, nyo_
  real(WP), dimension(:), allocatable :: x,y
  real(WP), dimension(:), allocatable :: xm,ym
  real(WP) :: dx, dy, dxi, dyi

end module grid

!=========================!
!      Create Grid        !
!=========================!
subroutine grid_create
  use grid
  use io
  implicit none
  
  integer :: rem
  integer :: i,j

  ! Read grid size
  call read_input('nx',nx)
  call read_input('ny',ny)

  ! Read the domain size
  call read_input('xmin',xmin)
  call read_input('xmax',xmax)
  call read_input('ymin',ymin)
  call read_input('ymax',ymax)

  ! Global index
  imin = 1; imax = nx
  jmin = 1; jmax = ny

  ! x local index 
  nx_ = nx/px
  rem = mod(nx,px)
  if (rankx.lt.rem) nx_ = nx_+1
  imin_ = imin + rankx*(nx/px) + min(rankx,rem)
  imax_ = imin_ + nx_ - 1

  ! y local index 
  ny_ = ny/py
  rem = mod(ny,py)
  if (ranky.lt.rem) ny_ = ny_ + 1    
  jmin_ = jmin + ranky*(ny/py) + min(ranky,rem)
  jmax_ = jmin_ + ny_ - 1

  ! Ghost cells
  imino_ = imin_ - nghost
  imaxo_ = imax_ + nghost
  jmino_ = jmin_ - nghost
  jmaxo_ = jmax_ + nghost
  nxo_   = imaxo_ - imino_ + 1
  nyo_   = jmaxo_ - jmino_ + 1

  ! X and Y values
  allocate(x(imino_:imaxo_+1))
  do i=imino_,imaxo_+1
     x(i)= & 
          xmin*(real(i)-real(imax+1))/(real(imin)-real(imax+1)) + &
          xmax*(real(imin)-real(i))/(real(imin)-real(imax+1))
  end do
  allocate(y(jmino_:jmaxo_+1))
  do j=jmino_,jmaxo_+1
     y(j)= & 
          ymin*(real(j)-real(jmax+1))/(real(jmin)-real(jmax+1)) + &
          ymax*(real(jmin)-real(j))/(real(jmin)-real(jmax+1))
  end do

  ! Cell sizes
  dx=(xmax-xmin)/nx
  dy=(ymax-ymin)/ny
  dxi=1.0_WP/dx
  dyi=1.0_WP/dy

  ! Xm and Ym valus
  allocate(xm(imino_:imaxo_))
  do i=imino_,imaxo_-1
     xm(i)=(x(i+1)+x(i))/2.0_WP
  end do
  xm(imaxo_)=xm(imaxo_-1)+dx
  allocate(ym(jmino_:jmaxo_))
  do j=jmino_,jmaxo_-1
     ym(j)=(y(j+1)+y(j))/2.0_WP
  end do
  ym(jmaxo_)=ym(jmaxo_-1)+dy

end subroutine grid_create



