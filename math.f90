! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module math
  use communication
  use polynomial
  use variables
  use basis
  use grid
  use io
  implicit none

  real(WP), parameter :: Pi = &
       3.14159265358979323846264338327950288419716939937510

  real(QP), parameter :: RombergConverge = &
       0.00000000000000000000000000000000000000000000000001_QP

contains

  !=========================================!
  ! Operator AtV : computes vec(A)^t*vec(B) !
  ! where A and B are matrices, and vec(A)  !
  ! and vec(B) are their vector forms       !
  !=========================================!
  function AtB(A,B)
    implicit none

    ! Inputs
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_), intent(in) :: A
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_), intent(in) :: B

    ! Variables
    real(WP) :: AtB_local

    ! Outputs
    real(WP) :: AtB

    ! Zero Solution
    AtB_local = 0.0_WP

    ! Compute A^t*B on interior of domain
    AtB_local = sum(A(imin_:imax_,jmin_:jmax_)*B(imin_:imax_,jmin_:jmax_))

    ! Communicate
    call sum_real(AtB_local,AtB)

    return
  end function AtB


  !===================================!
  ! Operator ApBC(f) : computes A+B*C !
  ! for matrices A & C and constant B !
  !===================================!
  function ApBC(A,B,C)
    implicit none

    ! Inputs
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_), intent(in) :: A
    real(WP),                                         intent(in) :: B
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_), intent(in) :: C

    ! Outputs
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: ApBC

    ! Zero ApBC
    ApBC = 0.0_WP

    ! Calculate laplacian of A on interior of domain?
    ApBC = A + B*C

    return
  end function ApBC

  !=================================================!
  ! Evaluate String of Polynomials at Location zeta !
  !=================================================!
  function poly_eval(Ak,zeta)
    implicit none

    real(WP), dimension(1:ndof), intent(in) :: Ak
    real(WP),   intent(in) :: zeta
    real(QP) :: poly_eval

    integer :: i,t

    ! Evaluate polynomial
    poly_eval=0.0_QP
    do t=0,order
       do i=1,phi(t)%n
          poly_eval = poly_eval + real(Ak(t+1),QP)*phi(t)%a(i)*real(zeta,QP)**phi(t)%p(i)
       end do
    end do

    return
  end function poly_eval

  !==============================================!
  ! Evaluate Polynomials for Array at Location x !
  !==============================================!
  function poly_eval2(Ak,x)
    implicit none

    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,1:ndof), intent(in) :: Ak
    real(WP), intent(in) :: x
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: poly_eval2
    integer :: i,t

    ! Evaluate polynomial
    poly_eval2 = 0.0_WP
    do t=0,order
       do i=1,phi(t)%n
          poly_eval2 = poly_eval2 + Ak(:,:,t+1)*real(phi(t)%a(i),WP)*x**phi(t)%p(i)
       end do
    end do

    return
  end function poly_eval2

  !==============================================!
  !  Evaluate Polynomials for Array at Quadpt i  !
  !==============================================!
  function poly_eval3(Ak,i)
    implicit none

    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,1:ndof), intent(in) :: Ak
    integer, intent(in) :: i
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: poly_eval3
    integer :: k

    ! Evaluate polynomial
    poly_eval3 = 0.0_WP
    do k = 1,ndof
       poly_eval3 = poly_eval3 + Ak(:,:,k)*Quad_bas(k,i)
    end do

    return
  end function poly_eval3

  !===============================================!
  ! Calculate the Level Set Function for a Circle !
  !===============================================!
  function circle_calc(r,x,y)
    implicit none

    real(WP),   intent(in) :: r,x,y
    real(WP) :: circle_calc

    circle_calc = r - sqrt(x**2 + y**2)

    return
  end function circle_calc

  !==================================!
  ! Perform a Sigmoid Transformation !
  !==================================!
  function sigmoid(Gin,eps)
    implicit none

    real(WP),   intent(in) :: Gin,eps
    real(WP) :: sigmoid

    sigmoid = 1.0_WP/(1.0_WP+exp(-2.0_WP*Gin/eps))

    return
  end function sigmoid

  !==============================================!
  !    Creation of Sigmoid Transformed Circle    !
  !==============================================!
  function UCcircle_romberg(xk,yk,func,xx,yy,r,eps,A,B)
    implicit none
    real(WP), intent(in) :: xx,yy,r,A,B,eps
    real(WP), dimension(1:ndof), intent(in) :: xk,yk
    type(poly), intent(in) :: func
    real(WP) :: UCcircle_romberg
    integer :: i,ni,m
    real(QP) :: best, best2
    real(WP) :: aa,bb,h
    real(WP), dimension(3) :: xl

    ! Romberg Integration
    ! Uses Simpson's Rule as the basis
    ! Integrate until convergence is reached
    ri = 0.0_QP; ni = 1
    best = 1.0_WP; best2 = 0.0_WP
    do while (abs(best-best2).gt.RombergConverge)
       do i = 1,ni
          aa = A + real((i-1),WP)*(B-A)/real(ni,WP)
          bb = aa + (B-A)/real(ni,WP)
          xl = [aa,0.5_WP*(aa+bb),bb]
          h = 0.5_WP*(bb-aa)
          ri(ni,1) = ri(ni,1) + (h/3.0_WP)* &
               (real(sigmoid(circle_calc(r,(xx-real(poly_eval(xk,xl(1)),WP)), &
               (yy-real(poly_eval(yk,xl(1)),WP))),eps),QP)*eval(func,xl(1)) + &
               4.0_QP*real(sigmoid(circle_calc(r,(xx-real(poly_eval(xk,xl(2)),WP)), &
               (yy-real(poly_eval(yk,xl(2)),WP))),eps),QP)*eval(func,xl(2)) + &
               (real(sigmoid(circle_calc(r,(xx-real(poly_eval(xk,xl(3)),WP)), &
               (yy-real(poly_eval(yk,xl(3)),WP))),eps),QP))*eval(func,xl(3)))
       end do
       ! Iterate using Richardson Extrapolation
       if (ni.gt.2) then
          do m = 2,ni
             ri((ni+1-m),m) = (1.0_QP/((4.0_QP**m)-1.0_QP))* &
                  ((4.0_QP**real(m,QP))*ri((ni+2-m),(m-1))-ri((ni+1-m),(m-1)))
          end do
          best = ri(1,ni); best2 = ri(2,(ni-1))
       end if
       ni = ni+1
    end do

    UCcircle_romberg = real(best,WP)

  end function UCcircle_romberg

  !=============================================================!
  !          Vectorized Calculation of Inverse Density          !
  !=============================================================!
  function rhoI_romberg(rho_In,func)
    implicit none
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,1:ndof), intent(in) :: rho_In
    type(poly), intent(in) :: func
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_) :: rhoI_romberg, best, best2
    real(WP) :: aa, bb, Res, Res_local
    real(WP), dimension(3) :: xl
    integer :: i,ni,m

    ! Romberg Integration
    ! Uses Simpson's Rule as the basis
    ! Integrate until convergence is reached
    riV = 0.0_WP; ni = 1
    best = 1.0_WP; best2 = 0.0_WP; Res = 1.0_WP
    do while (Res.gt.RombergConverge)
       do i = 1,ni
          aa = zetamin + real((i-1),WP)*(zetamax-zetamin)/real(ni,WP)
          bb = aa + (zetamax-zetamin)/real(ni,WP)
          xl = [aa,0.5_WP*(aa+bb),bb]
          riV(:,:,ni,1) = riV(:,:,ni,1) + (0.5_WP*(bb-aa)/3.0_WP)* &
               (real(eval(func,xl(1)),WP)/poly_eval2(rho_In,xl(1)) + &
               4.0_WP*real(eval(func,xl(2)),WP)/poly_eval2(rho_In,xl(2)) + &
               real(eval(func,xl(3)),WP)/poly_eval2(rho_In,xl(3)))
       end do
       ! Iterate using Richardson Extrapolation
       if (ni.gt.2) then
          do m = 2,ni
             riV(:,:,(ni+1-m),m) = (1.0_WP/((4.0_WP**m)-1.0_WP))* &
                  ((4.0_WP**real(m,WP))*riV(:,:,(ni+2-m),(m-1))-riV(:,:,(ni+1-m),(m-1)))
          end do
          best(:,:) = riV(:,:,1,ni); best2(:,:) = riV(:,:,2,(ni-1))
       end if
       ni = ni+1
       Res_local = maxval(abs(best-best2))
       call max_real(Res_local,Res)
    end do

    rhoI_romberg = best

  end function rhoI_romberg

  !==============================================!
  !          Calculate a standard normal         !
  !==============================================!
  function Normal_romberg(Nk,Nxk,Nyk,func,A,B)
    implicit none
    real(WP), intent(in) :: A,B
    real(WP), dimension(1:ndof), intent(in) :: Nk,Nxk,Nyk
    type(poly), intent(in) :: func
    real(WP) :: Normal_romberg
    integer :: i,ni,m
    real(QP) :: best, best2
    real(WP) :: aa,bb,h
    real(WP), dimension(3) :: xl

    ! Romberg Integration
    ! Uses Simpson's Rule as the basis
    ! Integrate until convergence is reached
    ri = 0.0_QP; ni = 1
    best = 1.0_WP; best2 = 0.0_WP
    do while (abs(best-best2).gt.RombergConverge)
       do i = 1,ni
          aa = A + real((i-1),WP)*(B-A)/real(ni,WP)
          bb = aa + (B-A)/real(ni,WP)
          xl = [aa,0.5_WP*(aa+bb),bb]
          h = 0.5_WP*(bb-aa)
          ri(ni,1) = ri(ni,1) + (h/3.0_WP)* &
               ((poly_eval(Nk,xl(1))/sqrt(poly_eval(Nxk,xl(1))**2 + poly_eval(Nyk,xl(1))**2))*eval(func,xl(1)) + &
               4.0_QP*(poly_eval(Nk,xl(2))/sqrt(poly_eval(Nxk,xl(2))**2 + poly_eval(Nyk,xl(2))**2))*eval(func,xl(2)) + &
               (poly_eval(Nk,xl(3))/sqrt(poly_eval(Nxk,xl(3))**2 + poly_eval(Nyk,xl(3))**2))*eval(func,xl(3)))
       end do
       ! Iterate using Richardson Extrapolation
       if (ni.gt.2) then
          do m = 2,ni
             ri((ni+1-m),m) = (1.0_QP/((4.0_QP**m)-1.0_QP))* &
                  ((4.0_QP**real(m,QP))*ri((ni+2-m),(m-1))-ri((ni+1-m),(m-1)))
          end do
          best = ri(1,ni); best2 = ri(2,(ni-1))
       end if
       ni = ni+1
    end do

    Normal_romberg = real(best,WP)

  end function Normal_romberg

  !=====================================!
  !        Output Zalesaks Disk         !
  !=====================================!
  function Zalesaks(xo,yo)
    implicit none

    real(WP), intent(in) :: xo, yo
    integer :: i,j,br
    real(WP) :: Nw,Nh,epsZ
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,1:ndof) :: notch
    real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_,1:ndof) :: Zalesaks

    ! Zero the Notch
    notch = 0.0_WP
    Zalesaks = 0.0_WP
    D  = 0.4_WP

    ! Set Disk Attributes
    Nw = 0.2_WP; Nh = 0.65_WP
    br = floor(Nw*0.5_WP*D/max(dx,dy))-4

    ! Form the disk
    do j=jmin_,jmax_
       do i=imin_,imax_
          !  Map a circle
          Zalesaks(i,j,1)=(0.5_WP*D)-sqrt((xm(i)-xo)**2+(ym(j)-yo)**2)
          if (Zalesaks(i,j,1).ge.(0.0_WP)) then
             Zalesaks(i,j,1)=1.0_WP
          else
             Zalesaks(i,j,1)=0.0_WP
          end if
          !  Form a Notch
          if ((abs(xm(i)-xo).lt.(Nw*0.5_WP*D)).and.((ym(j)-yo).lt.(Nh*0.5_WP*D)).and. &
               ((Zalesaks(i,j,1)-1.0_WP).ge.epsilon(0.0_WP))) notch(i,j,1) = -1.0_WP
       end do
    end do

    ! Communicate
    ! call comm_borders(Zalesaks)
    ! call comm_borders(notch)

    ! Shift everything vertically and rescale
    Zalesaks(:,:,1)=Zalesaks(:,:,1)+notch(:,:,1); Zalesaks(:,:,1)=2.0_WP*Zalesaks(:,:,1)
    Zalesaks(:,:,1)=Zalesaks(:,:,1)-1.0_WP; Zalesaks(:,:,1)=0.5_WP*Zalesaks(:,:,1)

    ! Transform with a Sigmoid
    epsZ = 2.0_WP*max(dx,dy)
    Zalesaks(:,:,1)=1.0_WP/(1.0_WP+exp(-Zalesaks(:,:,1)/(0.5_WP*epsZ)))

    return
  end function Zalesaks

  !==================================!
  ! Inverse the linear system : AX=B !
  !==================================!
  subroutine solve_linear_system(A,B,X,n)
    implicit none

    integer, intent(in) :: n
    real(WP), dimension(n,n) :: A
    real(WP), dimension(n) :: B
    real(WP), dimension(n) :: X
    integer :: i,j,l

    ! Forward elimination
    do i=1,n
       ! Pivoting
       j = i-1+maxloc(abs(A(i:n,i)),1)
       X = A(i,:)
       A(i,:) = A(j,:)
       A(j,:) = X
       X(1) = B(i)
       B(i) = B(j)
       B(j) = X(1)
       ! Elimination
       B(i) = B(i) / A(i,i)
       A(i,:) = A(i,:) / A(i,i)
       do l=i+1,n
          B(l) = B(l) - A(l,i)*B(i)
          A(l,:) = A(l,:) - A(l,i)*A(i,:)
       end do
    end do

    ! Backward substitution
    do i=n,1,-1
       X(i) = B(i)
       do l=i+1,n
          X(i) = X(i) - A(i,l)*X(l)
       end do
    end do

    return
  end subroutine solve_linear_system

  ! Inverse the linear system : AX=B
  ! Safe version with epsilon
  subroutine solve_linear_system_safe(A,B,X,n)
    implicit none

    integer, intent(in) :: n
    real(WP), dimension(n,n) :: A
    real(WP), dimension(n) :: B
    real(WP), dimension(n) :: X
    integer :: i,j,l

    ! Forward elimination
    do i=1,n
       ! Pivoting
       j = i-1+maxloc(abs(A(i:n,i)),1)
       X = A(i,:)
       A(i,:) = A(j,:)
       A(j,:) = X
       X(1) = B(i)
       B(i) = B(j)
       B(j) = X(1)
       ! Elimination
       B(i) = B(i) / (A(i,i)+epsilon(A(i,i)))
       A(i,:) = A(i,:) / (A(i,i)+epsilon(A(i,i)))
       do l=i+1,n
          B(l) = B(l) - A(l,i)*B(i)
          A(l,:) = A(l,:) - A(l,i)*A(i,:)
       end do
    end do

    ! Backward substitution
    do i=n,1,-1
       X(i) = B(i)
       do l=i+1,n
          X(i) = X(i) - A(i,l)*X(l)
       end do
    end do

    return
  end subroutine solve_linear_system_safe

  ! Inverse matrix A safely using QR
  subroutine QRinverse_matrix(n,A,iA)
    implicit none

    integer, intent(in) :: n
    real(WP), dimension(n,n) :: A,iA

    integer :: i,j
    real(WP) :: norm,tol
    real(WP), dimension(n,n) :: W,R,Q,iR

    ! Set tolerance
    Q(1:n,1:n)=matmul(transpose(A(1:n,1:n)),A(1:n,1:n))
    tol=0.0_WP
    do i=1,n
       tol=max(tol,abs(Q(i,i)))
    end do
    tol=sqrt(tol)*1.E-8_WP

    ! Modified QR decomposition
    Q(1:n,1:n)=0.0_WP
    R(1:n,1:n)=0.0_WP
    do j=1,n
       W(1:n,j)=A(1:n,j)
       do i=1,j-1
          R(i,j)=dot_product(W(1:n,j),Q(1:n,i))
          W(1:n,j)=W(1:n,j)-R(i,j)*Q(1:n,i)
       end do
       norm=sqrt(dot_product(W(1:n,j),w(1:n,j)))
       if (norm.gt.tol) then
          Q(1:n,j)=W(1:n,j)/norm
          R(j,j)=norm
       end if
    end do

    ! R inverse
    iR(1:n,1:n)=0.0_WP
    do j=1,n
       do i=1,j-1
          iR(i,j)=dot_product(iR(i,1:j-1),R(1:j-1,j))
       end do
       norm=R(j,j)
       if (norm.gt.tol) then
          iR(1:j-1,j)=-iR(1:j-1,j)/norm
          iR(j,j)=1.0_WP/norm
       else
          iR(1:j-1,j)=0.0_WP
          iR(j,j)=0.0_WP
       end if
    end do

    ! A inverse
    iA(1:n,1:n)=matmul(iR(1:n,1:n),transpose(Q(1:n,1:n)))

    return
  end subroutine QRinverse_matrix

end module math

!===================================!
!    Calculate HOUC Coefficients    !
!===================================!
subroutine houc_init
  use math
  implicit none

  ! Variables
  integer :: i,j
  real(WP), dimension(:,:), pointer :: Mh, Mh2, Bh

  ! Allocate Matrices
  allocate(houc_xp(nhouc)); allocate(houc_xm(nhouc))
  allocate(houc_yp(nhouc)); allocate(houc_ym(nhouc))
  allocate(Mh (nhouc,nhouc)); allocate(Mh2(nhouc,nhouc))
  allocate(Bh (nhouc,1))

  ! Calculate HOUC weights for x
  ! Populate M matrix
  Mh = 0.0_WP
  do i = 1,nhouc
     do j = 1,nhouc
        Mh(i,j) = (((-real(nhouc,WP)/2.0_WP+real(j-1,WP))*dx)**(i-1))/real(factorial(i-1),WP)
     end do
  end do

  ! Populate the B matrix
  Bh = 0.0_WP
  Bh(1,1) = -1.0_WP ! the rest are zeros

  ! Solve for the coefficients
  ! Ordered as [A(i-5/2) A(i-3/2) ... A(i+1/2) A(i+3/2)]
  call solve_linear_system(-Mh,Bh,houc_xp,nhouc)

  ! Flip the array
  do i = 1,nhouc
     houc_xm(nhouc+1-i) = houc_xp(i)
  end do

  ! Calculate HOUC weights for y
  ! Populate M matrix
  Mh = 0.0_WP
  do i = 1,nhouc
     do j = 1,nhouc
        Mh(i,j) = (((-real(nhouc,WP)/2.0_WP+(j-1))*dy)**(i-1))/real(factorial(i-1),WP)
     end do
  end do

  ! Populate the B matrix
  Bh = 0.0_WP
  Bh(1,1) = -1.0_WP ! the rest are zeros

  ! Solve for the coefficients
  ! Ordered as [A(i-5/2) A(i-3/2) ... A(i+1/2) A(i+3/2)]
  call solve_linear_system(-Mh,Bh,houc_yp,nhouc)

  ! Flip the array
  do i = 1,nhouc
     houc_ym(nhouc+1-i) = houc_yp(i)
  end do

  ! Deallocate the matrices
  deallocate(Mh); nullify(Mh)
  deallocate(Bh); nullify(Bh)

  return
end subroutine houc_init

!===============================================!
!          Calculate Quadrature Points          !
!===============================================!
subroutine quadrature_init
  use variables
  use basis
  use grid
  use io
  implicit none

  ! Variables
  real(QP), parameter :: pi = &
       3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679
  real(QP) :: z, f, df, dz
  integer  :: i, j, k
  real(QP), allocatable :: p0(:), p1(:), tmp(:)
  real(QP), dimension(quadpts) :: RgaussQ
  real(QP), dimension(1:ndof,1:quadpts) :: Quad_buf

  ! Allocate weights and points
  allocate(Wgauss(1:quadpts)); Wgauss = 0.0_WP
  allocate(Rgauss(1:quadpts)); Rgauss = 0.0_WP
  allocate(Quad_bas(1:ndof,1:quadpts)); Quad_bas = 0.0_WP

  ! Initialize arrays
  RgaussQ = 0.0_QP; Quad_buf = 0.0_QP

  ! Allocate arrays
  p0 = [1.0_QP]
  p1 = [1.0_QP, 0.0_QP]

  ! Calculate weights and abscissae
  do k = 2,quadpts
     tmp = ((2.0_QP*real(k,QP)-1.0_QP)*[p1,0.0_QP]-(real(k,QP)-1.0_QP)*[0.0_QP,0.0_QP,p0])/real(k,QP)
     p0 = p1; p1 = tmp
  end do
  do i = 1,quadpts
     z = cos(pi*(real(i,QP)-0.25_QP)/(real(quadpts,QP)+0.5_QP))
     do j = 1, 10000
        f = p1(1); df = 0.0_QP
        do k = 2,size(p1)
           df = f + z*df
           f  = p1(k) + z*f
        end do
        dz =  f/df
        z = z - dz
        if (abs(dz).lt.10.0_QP*epsilon(dz)) exit
     end do
     RgaussQ(i) = z
     Rgauss(i) = real(z,WP)
     Wgauss(i) = real(2.0_QP/((1.0_QP-(z**2))*(df**2)),WP)
  end do

  ! Save quadrature calculation of each basis function for each point
  do k = 1,ndof
     do i = 1,quadpts
        Quad_buf(k,i) = evalQ(phi(k-1),RgaussQ(i))
     end do
  end do

  Quad_bas = real(Quad_buf,WP)

  ! Perform Error Check and Send Warning
  if (rank.eq.root) then
     if (abs(sum(Wgauss)-(zetamax-zetamin)).gt.epsilon(0.0_WP)) then
        print*,'!======================== ALERT ========================!'
        print*,'!=======================================================!'
        print*,'!==            Quadrature Calculation Issue           ==!'
        print*,'!=======================================================!'
        print*,'   Quadrature weights should add up to zeta width'
        print*,'   Weight information '
        print*,'      Minimum Value  = ',minval(Wgauss)
        print*,'      Maximum Value  = ',maxval(Wgauss)
        print*,'      Sum of Weights = ',sum(Wgauss)
        print*,'!=======================================================!'
        print*,'!==        Simulation Values May Be Inaccurate        ==!'
        print*,'!=======================================================!'
     end if
  end if

end subroutine quadrature_init










