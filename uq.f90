! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module uq
  use variables
  use basis
  use grid
  use communication
  implicit none

end module uq

!=========================!
!      Initialization     !
!=========================!
subroutine uq_init
  use uq
  use io
  implicit none

  ! Check for uncertainty
  if (.not.is_uq) then
     ndof = 1
     ndof_nu = 1
  end if

  ! Allocate arrays
  ! Viscosity
  allocate(nu    (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(nu_lef(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(nu_bot(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(nuI   (1:ndof))
  allocate(nuE   (1:ndof))
  ! Density
  allocate(rhok   (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(rhoIk  (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(iD_lef (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(iD_bot (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(rho_lef(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(rho_bot(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Rco    (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(rhoI   (1:ndof))
  allocate(rhoE   (1:ndof))
  ! Gravity
  allocate(Ks (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Kx (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Ky (imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Kxx(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Kyy(imino_:imaxo_,jmino_:jmaxo_,1:ndof))
  allocate(Gr(1:ndof))

  ! Surface tension
  allocate(gammak(1:ndof))

  ! Initialize viscosity
  nuI = 0.0_WP; nuE = 0.0_WP
  nuI(1) = nu_io; nuE(1) = nu_ioE
  if (ndof.gt.1) nuI(2) = sigmaVI
  if (ndof.gt.1) nuE(2) = sigmaVE

  ! Initialize density
  rhoI = 0.0_WP; rhoE = 0.0_WP
  rhoI(1) = rho_io; rhoE(1) = rho_ioE
  if (ndof.gt.1) rhoI(2) = sigmaDI
  if (ndof.gt.1) rhoE(2) = sigmaDE

  ! Initialize gravity
  Gr = 0.0_WP;   Gr(1) = grav_acc
  if (ndof.gt.1) Gr(2) = grav_var

  ! Initialize surface tension
  gammak = 0.0_WP; gammak(1) = gamma
  if (ndof.gt.1)   gammak(2) = sigmaG

  return
end subroutine uq_init

!=========================================================!
!     Single Phase Density & Viscosity Initialization     !
!=========================================================!
subroutine density_init
  use uq
  use variables
  use io
  implicit none

  rhok = 0.0_WP
  rhok = rho_io
  rhoIk = 0.0_WP
  rhoIk = 1.0_WP/rho_io


  return
end subroutine density_init

